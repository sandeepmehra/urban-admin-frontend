import React, {useState, useEffect} from 'react'
import style from "./Inventory.module.css";
import { useDispatch, useSelector } from 'react-redux';
import { ShowStarRating } from '../../components/StarRating/ShowStarRating';
import { Accordion } from '../../components/Accordion';
import { Outlet } from 'react-router-dom';
import { Loader } from '../../components/Loader/Loader';
import { resInventoryData, resUnitData } from '../../store/reducer/inventoryReducer';
import { setError } from '../../store/reducer/errorReducer';

const Inventory = () => {
    const {  inventory, isLoading, isError, error } = useSelector((state) => state.inventorySlice);
    console.log("inventory: ", inventory);
    const dispatch = useDispatch();
    const [isOpen, setIsOpen] = useState([]);
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(()=>{
        dispatch(resInventoryData());
        dispatch(resUnitData());
    }, [dispatch]);
    return (
        <>
            <div className={style.main}>
                <h1 className={style.inventory_heading}>Inventory</h1>
                {!isLoading ? 
                    <table className={style.table}>
                        <thead className={style.thead}>
                                <tr className={style.tr}>
                                <th className={style.th}>Seller name</th>
                                <th className={style.th}>Product name</th>
                                <th className={style.th}>SKU</th>
                                <th className={style.th}>Price</th>
                                <th className={style.th}>Status</th>
                                <th className={style.th}>Rating</th>
                                <th className={style.th}></th>
                            </tr>
                        </thead>
                        {inventory && inventory.map((element, index)=>(<tbody className={style.tbody} key={index}>
                            <tr className={`${style.tr} ${style.parent_row}`}>
                                <td className={style.td}>{element.seller_name}</td>
                                <td className={style.td}>{element.product_name}</td>
                                <td className={style.td}>{element.sku}</td>
                                <td className={style.td}>&#x20B9;{element.price}</td>
                                <td className={style.td}>{element.status}</td>
                                <td className={style.td}>
                                <div className={style.status_costumer}>
                                        <ShowStarRating value={element.rating} />
                                    </div> 
                                </td>
                                <td className={style.td}> 
                                    <Accordion setIsOpen={setIsOpen} isOpen={isOpen} id={element.id}/>
                                </td>
                            </tr>
                            { isOpen.includes(element.id) && ( 
                                <tr className={style.detail_shown}>
                                    <td colSpan="7">
                                        <Outlet /> 
                                    </td>
                                </tr>
                                )
                            }      
                        </tbody>))}
                    </table>
                : <Loader />}
            </div>    
        </>
    )
}

export default Inventory;
