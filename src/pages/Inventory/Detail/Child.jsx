import React, {useEffect, useState} from 'react';
import style from "./InventoryDetail.module.css";
import FeatherIcon from 'feather-icons-react';
import Dropdown from '../../../components/Dropdown/Dropdown';
import { InventoryButton } from './Button';
import { diff } from '../../../utils/CompareTwoObject';



const Child = ({
    inventory, 
    updatedHandler, 
    inputHandler, 
    removeVideo, 
    removeImage, 
    addSeoKeyword, 
    removeSeoKeyword, 
    handleApproveClick, 
    handleRejectClick, 
    keywords, 
    setKeywords,
    currentData,
    setCurrentData,
    units
}) => {
    let unitIndex = units.findIndex(item => item.value === inventory.measurement_unit);
    const [unit, setUnit] = useState({
        value : inventory?.measurement_unit,
        label : inventory?.measurement_unit,
        key: unitIndex
    });
    const chooseUnit = (value, e) => {
        setUnit({
            ...unit,
            value: value.value,
            label: value.label,
            key: value.key
        });
        setCurrentData({
            ...currentData,
            'measurement_unit' : value.value, 
        });
    }
 
    useEffect(()=>{
        setCurrentData(inventory);
    }, [inventory, setCurrentData]);
    return (
        <>
            <div>
                <table className={style.inventory_table}>
                    <tbody>
                        <tr className={style.inventory_tr}>
                            <td style={{"width": "45%"}}>
                                <div className={`${style.row_div} ${style.row_pt}`}>
                                    <div className={`${style.inventory_th_new}`}>Product Name</div>
                                    <div className={`${style.inventory_td} `}>
                                        <input 
                                            type={'text'} 
                                            className={style.input_inventory} 
                                            name={"product_name"} 
                                            defaultValue={currentData.product_name} 
                                            onChange={inputHandler}
                                        />
                                    </div>
                                </div>
                                <div className={`${style.row_div} ${style.row_pt}`}>
                                    <div className={`${style.inventory_th_new}`}>Product Category</div>
                                    <div className={style.inventory_td}>
                                        <input 
                                            type={'text'} 
                                            className={style.input_inventory} 
                                            name={"category"} 
                                            defaultValue={currentData.category} 
                                            onChange={inputHandler}
                                        />
                                    </div>
                                </div>
                                <div className={`${style.row_div} ${style.row_pt}`}>
                                    <div className={`${style.inventory_th_new} `}>Inventory</div>
                                    <div className={style.inventory_td}>
                                        <input 
                                            type={'text'} 
                                            className={style.input_inventory} 
                                            name={"quantity"} 
                                            defaultValue={currentData.quantity} 
                                            onChange={inputHandler} 
                                        />
                                    </div>
                                </div>
                                <div className={`${style.row_div} ${style.row_pt}`}>
                                    <div className={`${style.inventory_th_new} `}>Unit</div>
                                    <div className={style.inventory_td}>
                                        <div className={style.wrapper}>
                                            <Dropdown 
                                                menuHeight={150}
                                                optionsData={units}
                                                placeholder={unit.value}
                                                defaultValue={units[unitIndex]}
                                                chooseUnitData={(e)=>chooseUnit(e)}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className={`${style.row_div} ${style.row_pt}`}>
                                    <div className={`${style.inventory_th_new} `}>Country of Origin</div>
                                    <div className={style.inventory_td}>
                                        <input 
                                            type={'text'} 
                                            className={style.input_inventory} 
                                            name={"country_of_origin"}  
                                            defaultValue={currentData.country_of_origin} 
                                            onChange={inputHandler} 
                                        />
                                    </div>
                                </div>
                                <div className={`${style.row_div} ${style.row_pt}`}>
                                    <div className={`${style.inventory_th_new}`}>SKU</div>
                                    <div className={style.inventory_td}>
                                        <input 
                                            type={'text'} 
                                            className={style.input_inventory} 
                                            name={"sku"} 
                                            defaultValue={currentData.sku} 
                                            onChange={inputHandler} 
                                        />
                                    </div>
                                </div>
                                <div className={`${style.row_div} ${style.row_pt}`}>
                                    <div className={`${style.inventory_th_new}`}>HSN Code</div>
                                    <div className={style.inventory_td}>
                                        <input 
                                            type={'text'} 
                                            className={style.input_inventory} 
                                            name={"hsn_code"} 
                                            defaultValue={currentData.hsn_code} 
                                            onChange={inputHandler} 
                                        />
                                    </div>
                                </div>
                                <div className={`${style.row_div} ${style.row_pt}`}>
                                    <div className={`${style.inventory_th_new} `}>
                                        <div className={style.inventory_seo}>
                                            SEO Keywords
                                        </div>
                                    </div>
                                    <div className={style.inventory_td}>
                                        <form onSubmit={(e)=>addSeoKeyword(e)}>
                                            <input 
                                                type="text" 
                                                name={"seo_keywords"}
                                                value={keywords}
                                                onChange={e => setKeywords(e.target.value)}
                                                className={style.input_keyword} 
                                                placeholder='Enter Keyword' 
                                                autoComplete='off'
                                            />
                                            <button type="submit" className={style.btn_keyword}>save</button>
                                        </form>
                                        <div className={`${style.keyword_text} ${style.keyword_suggestion_outer}`}>
                                            {currentData?.seo_keywords?.map((element, index)=>(
                                            <span className={style.keyword_suggestion} key={index}>
                                                {element}
                                                <FeatherIcon 
                                                    icon={"x"} 
                                                    color="red" 
                                                    size={"15"}
                                                    onClick={()=>removeSeoKeyword(index)}
                                                />
                                            </span>))}
                                        </div>
                                        
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div className={`${style.row_div} ${style.row_ptm}`}>
                                    <div className={`${style.inventory_th} ${style.th_width}`}>Product Images</div>
                                    <div className={style.inventory_td}>
                                        {currentData?.images?.map((element, index) =>(
                                            <div className={style.inventory_img_box} key={index}>
                                                <img className={style.inventory_image} src={element} width={"50"} height={"50"} alt='product_image' />
                                                <InventoryButton element={element} remove={removeImage} index={index}/>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                                <div className={`${style.row_div} ${style.row_ptm}`}>
                                    <div className={`${style.inventory_th} ${style.th_width}`}>Product Videos</div>
                                        <div className={style.inventory_td}>
                                            {currentData?.videos?.map((element, index) =>( 
                                                <div className={style.inventory_video_box} key={index}>
                                                    <video src={element} width="60" height="60" key={index} />
                                                    <InventoryButton element={element} remove={removeVideo} index={index} />
                                                </div>
                                            ))}
                                        </div>
                                </div>
                                <div className={`${style.row_div} ${style.row_ptm}`}>
                                    <div className={`${style.inventory_th} ${style.th_width}`}>Delivery Constraint</div>
                                    <div className={`${style.inventory_td} ${style.td_pl}`}>
                                        5 Km
                                    </div>
                                </div>
                                <div className={`${style.row_div} ${style.row_ptm}`}>
                                    <div className={`${style.inventory_th} ${style.th_width}`}>Varients</div>
                                    <div className={style.row_div}>
                                        <div className={style.row_column_div}>
                                            <div className={`${style.inventory_td} ${style.td_p}`}>Size</div>
                                            <div className={style.box_outer}>
                                                <input 
                                                    type={'text'} 
                                                    className={style.box_text} 
                                                    name={"measurement"} 
                                                    defaultValue={currentData?.measurement} 
                                                    onChange={inputHandler}
                                                />
                                                <small className={style.box_outer_text}>{unit.value}</small>
                                            </div>
                                        </div>
                                        <div className={`${style.row_column_div} ${style.td_pl10}`}>
                                            <div className={`${style.inventory_td} ${style.td_p}`}>Color</div>
                                            <span className={style.inventory_varients_circle_red}></span>   
                                        </div>
                                    </div>
                                </div>
                                <div className={style.inventory_buttons}>
                                    {(inventory?.action_taken !== 'true') &&(
                                        <>
                                            <button className={style.btn} onClick={()=>handleApproveClick(inventory.id)}>Approve</button>
                                            <button className={style.btn} onClick={()=>handleRejectClick(inventory.id)}>Reject</button>
                                        </>
                                    )}
                                    <button className={style.update_button} onClick={()=> updatedHandler(currentData.id, diff(inventory, currentData))}>Update</button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </>
    )
}

export default Child;
