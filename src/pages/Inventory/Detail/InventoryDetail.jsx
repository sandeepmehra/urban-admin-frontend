import React, {useEffect, useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import Child from './Child';
import { updateInventoryApprovedOrRejected, updateInventoryProductInfo } from '../../../store/reducer/inventoryReducer';
import { toast } from 'react-toastify';
import { setError } from '../../../store/reducer/errorReducer';


const InventoryDetail = () => {
    const {  inventory, units, isError, error } = useSelector((state) => state.inventorySlice);
    const params = useParams();
    const {id} = params;
    const [keywords, setKeywords] = useState('');
    const [currentData, setCurrentData] = useState({
        product_name: '',
        sku: '',
        hsn_code: '',
        category: '',
        color: '',
        measurement: '',
        measurement_unit: '',
        quantity: '',
        country_of_origin: '',
        seo_keywords: [],
        videos: [],
        images: [],
    });
    const dispatch = useDispatch();
    const handleApproveClick = (id) => {
        dispatch(updateInventoryApprovedOrRejected({id:id, value:'approve'}));
    }
    const handleRejectClick = (id) => {
        dispatch(updateInventoryApprovedOrRejected({id:id, value:'reject'}));
    }
    const addSeoKeyword = (e) => {
        e.preventDefault();
        if(keywords !== '' && currentData.seo_keywords.length < 5){
            const seo_keywords = [...currentData.seo_keywords, keywords];
            setCurrentData({
                ...currentData,
                seo_keywords
            });
            setKeywords('');
        }
    }
    const removeSeoKeyword = (index) => {
        setCurrentData({
            ...currentData, 
            seo_keywords:  currentData.seo_keywords.filter((_, i) => i !== index)
        }); 
    }
    const removeImage = (e, index) => {
        e.preventDefault();
        setCurrentData({
            ...currentData,
            images: currentData.images.filter((item, i) => i !== index)
        });
    }
    const removeVideo = (e, index) => {
        e.preventDefault();
        console.log('video ', index);
        setCurrentData({
            ...currentData,
            videos: currentData.videos.filter((item, i) =>  i !== index)
        });
    }
    const inputHandler = (e) => {
        setCurrentData({
            ...currentData,
            [e.target.name] : e.target.value, 
        });
    }
  
    const updatedHandler = (id, updatedDetail) => {
        if(updatedDetail === 'Please reload the page') {
            return toast.info('Please reload the page');
        }
        let newObj = {};
        for(let key in updatedDetail){ 
            if(key === 'added'){
                if(updatedDetail.added.seo_keywords){
                    newObj['seo_keywords'] = {1: updatedDetail.added.seo_keywords};
                }
            }
            if(key === 'deleted'){
                if(updatedDetail.deleted.images){       
                    newObj['images'] = updatedDetail.deleted.images;
                }
                if(updatedDetail.deleted.videos){
                    newObj['videos'] = updatedDetail.deleted.videos;
                } 
                if(updatedDetail.deleted.seo_keywords){
                    newObj['seo_keywords'] = {0: updatedDetail.deleted.seo_keywords};
                } 
            }
            if(key === 'updated'){
                for(let attribute in updatedDetail.updated){
                    if(attribute){
                        newObj[`${attribute}`] = updatedDetail.updated[`${attribute}`];
                    }
                }
            }
        }
        if(Object.entries(newObj).length !== 0){
            dispatch(updateInventoryProductInfo({id:id, updateValue: newObj}));
        } else {
            toast.info('Nothing to update');
        }
    } 
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    return (
        <>
            <div>
                <Child  
                    inventory={inventory.find(item => item.id === id)}
                    updatedHandler={updatedHandler}
                    inputHandler={inputHandler}
                    removeVideo={removeVideo}
                    removeImage={removeImage}
                    addSeoKeyword={addSeoKeyword}
                    removeSeoKeyword={removeSeoKeyword}
                    handleApproveClick={handleApproveClick}
                    handleRejectClick={handleRejectClick}
                    keywords={keywords}
                    setKeywords={setKeywords}
                    currentData={currentData}
                    setCurrentData={setCurrentData}
                    units={units}
                />
            </div>
        </>
    )
}

export default InventoryDetail;
