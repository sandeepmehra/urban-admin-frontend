import React from 'react';
import style from "./InventoryDetail.module.css";
import {AiOutlineDelete, AiOutlineEye} from 'react-icons/ai';

export const InventoryButton = ({element, remove, index}) => {
  return (
    <div className={style.inventory_edit}>
        <span className={style.inventory_link} onClick={(e)=>remove(e, index)}><AiOutlineDelete /></span>
        <span className={style.inventory_link} onClick={()=> window.open(element, "_blank")} ><AiOutlineEye /></span>
    </div>
  )
}
