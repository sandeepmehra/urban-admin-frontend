import React, { useEffect, useState } from 'react';
import style from "./Orderdetail.module.css";
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { setError } from '../../store/reducer/errorReducer';


const Orderdetail = () => {
    const {  orders, isError, error } = useSelector((state) => state.orderSlice);
    const params = useParams();
    const dispatch = useDispatch();
    const {id} = params;
    const [order, setOrder] = useState({});
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(() => {
        setOrder(orders?.find(item => item.id === id));
    }, [orders, id]);  
    return (
        <>
            <div className={style.main_card}>
            <table className={`${style.table} ${style.scroller}`}>
                <thead>
                    <tr>
                        <th className={`${style.th} ${style.pl}`}>Product Name</th>
                        <th className={style.th}>SKU</th>
                        <th className={style.th}>Qty</th>
                        <th className={style.th}>Dispatch date</th>
                        <th className={style.th}>Delivery date</th>
                        <th className={style.th}>Status</th>
                        <th className={`${style.th} ${style.pr}`}>Total</th>
                    </tr>
                </thead>
                <tbody>
                    {order?.order_items !== undefined && order.order_items?.map((element, index)=>(
                        <tr key={index}>
                            <td className={`${style.td} ${style.pl}`}>{element.product_name}</td>
                            <td className={style.td}>{element.sku}</td>
                            <td className={style.td}>{element.quantity}</td>
                            <td className={style.td}>{element.dispatch_date !== null && element.dispatch_date !== 'NA' ? moment(element.dispatch_date).format('DD-MM-YYYY'): 'NA'}</td>
                            <td className={style.td}>{element.delivery_date !== null && element.delivery_date !== 'NA' ? moment(element.delivery_date).format('DD-MM-YYYY'): 'NA'}</td>
                            <td className={style.td}>{element.status}</td>
                            <td className={`${style.td} ${style.pr}`}>&#x20B9;{element.total}</td>
                        </tr>
                    ))}
                    
                </tbody>
                
            </table>
            <div className={style.other_details}>
                <div className={style.other_heading}>Other Details</div> 
                <div className={style.other_details_container}>
                    <div className={style.row_p}>
                        <div className={style.box_th}>
                            Delivery Address:
                            {order !== undefined && (
                                <span className={style.box_td}>
                                    {order.address}
                                </span>)
                            }
                        </div>
                    </div>
                    <div className={style.row_p}>
                        <div className={style.box_th}>
                            Total number of Items:
                            {order?.order_items !== undefined && (<span className={style.box_td}>{order.order_items.length}</span>)}
                        </div>
                    </div>
                    <div className={style.row_p}>
                        <div className={style.box_th}>Payment status:
                            <span className={style.box_td}>Done</span>
                        </div>
                    </div>
                    <div className={style.row_p}>
                        <div className={style.box_th}>
                            Payment method:
                            <span className={style.box_td}>Paypal</span>
                        </div>
                    </div>
                    </div>  
                </div>
            </div>
        </>
    )
}

export default Orderdetail;
