import React, {useState, useEffect} from 'react';
import style from"./Order.module.css";
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import { Outlet } from 'react-router-dom';
import { Accordion } from '../../components/Accordion';
import { Loader } from '../../components/Loader/Loader';
import { resOrderData } from '../../store/reducer/orderReducer';
import { setError } from '../../store/reducer/errorReducer';

const Order = () => {
    const {  orders, isLoading, isError, error } = useSelector((state) => state.orderSlice);
    console.log("orders: ", orders, isLoading, isError, error);
    const dispatch = useDispatch();
    const [isOpen, setIsOpen] = useState([]);
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(()=>{
        dispatch(resOrderData());
    }, [dispatch]); 
    return (
        <>
            <div className={style.main}>
                <h1 className={style.order_heading}>Orders</h1>
                {!isLoading ? 
                    <table className={style.table}>
                        <thead className={style.thead}>
                            <tr className={style.tr}>
                                <th className={style.th}>Order ID</th>
                                <th className={style.th}>Order Date</th>
                                <th className={style.th}>Customer</th>
                                <th className={style.th}>Payment method</th>
                                <th className={style.th}>Status</th>
                                <th className={style.th}>Total Order</th>
                                <th className={style.th}></th>
                            </tr>
                        </thead>
                        {orders && orders.map((element, index)=>(<tbody className={style.tbody} key={index}>
                            <tr className={style.tr}>
                                <td className={style.td}>#{element.id}</td>
                                <td className={style.td}>{moment(element.placed_on).format('DD-MM-YYYY')}</td>
                                <td className={style.td}>{element.customer}</td>
                                <td className={style.td}>{element.payment_mode}</td>
                                <td className={style.td}>{element.status}</td>
                                <td className={style.td}>&#x20B9;{element.total}</td>
                                <td className={style.td}>
                                <div className={style.status_costumer}>
                                        <span className={style.status_order_btn}>Remind</span>
                                        <div className={style.status_costumer}>
                                            <Accordion setIsOpen={setIsOpen} isOpen={isOpen} id={element.id}/>
                                        </div>
                                    </div> 
                                </td>
                            </tr>
                            {isOpen.includes(element.id) && ( 
                                <tr>
                                    <td colSpan="7">
                                        <Outlet />
                                    </td>
                                </tr>) 
                            }
                        </tbody>))}
                    </table>
                : <Loader />}
            </div>
        </>
    )
}

export default Order;
