import Cookies from 'js-cookie';
import { useCallback, useEffect } from 'react';
import {
    useSelector,
    useDispatch
} from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { ResetAuth, ResetAuthError } from '../../store/reducer/authReducer';
import { ResetCommissionError } from '../../store/reducer/commissionReducer';
import { ResetComplaintError } from '../../store/reducer/complaintReducer';
import { ResetCustomerError } from '../../store/reducer/customerReducer';
import { hideError } from '../../store/reducer/errorReducer';
import { ResetHomeError } from '../../store/reducer/homeReducer';
import { ResetInventoryError } from '../../store/reducer/inventoryReducer';
import { ResetNotificationError } from '../../store/reducer/notificationReducer';
import { ResetOrderError } from '../../store/reducer/orderReducer';
import { ResetSellerError } from '../../store/reducer/sellerReducer';


const ErrorBoundary = ({children}) => {
    const {error, isOpen} = useSelector((state) => state.error);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const clearError = useCallback(() => {
        dispatch(ResetOrderError());
        dispatch(ResetSellerError());
        dispatch(ResetAuthError());
        dispatch(ResetCommissionError());
        dispatch(ResetNotificationError());
        dispatch(ResetInventoryError());
        dispatch(ResetHomeError());
        dispatch(ResetCustomerError());
        dispatch(ResetComplaintError());
        dispatch(hideError());
    },[dispatch])
    useEffect(()=>{
        if(isOpen && error){
            if(error.statusCode === 0 || error.statusCode === undefined){
                toast.error("Network Error. Please check your connection!!!");
                navigate('/something-went-wrong');
            }else if(error.statusCode === 400){
                toast.error(error.message);
            }else if(error.statusCode === 401 || error.statusCode === 403){
                if(Cookies.get('at') !== undefined && Cookies.get('rt') !== undefined){
                    Cookies.remove('at');
                    Cookies.remove('rt');
                }
                dispatch(ResetAuth());  
            }else if(error.statusCode === 404){
                navigate(`/404`);
            }else if(error.statusCode >= 500){
                toast.error(error.message);
                navigate('/something-went-wrong');
            }else{
                toast.error("Something went wrong. Server Error!!!");
                navigate('/something-went-wrong');
            }
            clearError();
        }

    },[clearError, dispatch, error, isOpen, navigate])
    
    
    return children; 
}

export default ErrorBoundary;