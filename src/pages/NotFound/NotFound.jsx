import React from 'react';
import style from './NotFound.module.css';
import { Link } from 'react-router-dom';


const NotFound = () => {
    return (
        <div className={style.main}>
            <div>
                Oops!<strong className={style.heading}> 404 - Page Not Found</strong>
            </div>
            <div>
                <Link to={"/dashboard"}>
                    <input 
                        type="button" 
                        id={style.input_button}
                        className={style.btn} 
                        value={"Let's go to home"} 
                    />
                </Link>  
            </div>
        </div>
    )
}

export default NotFound
