import React, { useEffect, useState } from 'react';
import style from "./Commission.module.css";
import FeatherIcon from 'feather-icons-react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Loader } from '../../components/Loader/Loader';
import { addCommissionData, resCommissionsData } from '../../store/reducer/commissionReducer';
import { setError } from '../../store/reducer/errorReducer';

const Commission = () => {
    const [commission, setCommission] = useState('');
    const [checkedInfo, setCheckedInfo] = useState({
        selectedIdArray: [],
    });
    const { commissions, isLoading, isError, error } = useSelector((state) => state.commissionSlice);
    console.log("isLoading commissions", isLoading, commissions)
    const dispatch = useDispatch();
    const onChangeHandler = (e) => {
        const { value, checked } = e.target;
        const { selectedIdArray } = checkedInfo;
        if (checked) {
            setCheckedInfo({
                selectedIdArray: [...selectedIdArray, value],
            });
        } else {
            setCheckedInfo({
                selectedIdArray: selectedIdArray.filter((e) => e !== value),
            });
        }
    }
    const onSubmitHandler = (e) => {
        e.preventDefault();
        dispatch(addCommissionData({commission_percentage: Number(commission), sellers: checkedInfo.selectedIdArray}));
        setCommission('');
        setCheckedInfo({ 
            selectedIdArray: []
        });
    }
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(()=>{
        dispatch(resCommissionsData())
    }, [dispatch])
    return (
        <>
            <div className={style.main}>
                <span className={style.seller_heading}>Commission</span>
                <div className={style.c_box}>   
                    <span className={style.c_set_c}>Set Commission</span>
                    <form onSubmit={onSubmitHandler}>
                        <input 
                            type="text"     
                            className={style.input_commission}
                            value={commission} 
                            onChange={e => setCommission(e.target.value)}
                        />
                            
                        <button 
                            className={style.save_btn}
                        >
                            Save
                        </button>
                    </form>
                </div>
                {!isLoading ?  
                <table className={style.table}>
                    <tbody className={style.tbody}>
                        {commissions && commissions.map((element) => (
                            <tr className={style.tr} key={element.id}>
                                <td className={style.c_td}>
                                    <input type="checkbox" value={element.id} onChange={onChangeHandler} className={style.c_checkbox} />
                                </td>
                                <td className={style.c_td}>{element.name}</td>
                                <td className={style.c_td}>{element.email}</td>
                                <td className={style.c_td}>{element.mobileNumber}</td>
                                <td className={style.c_td}>{element.location}</td>
                                <td className={style.c_td}>
                                    <div className={style.status_seller}>
                                        <span className={`${element.verified=== true ? style.c_active : style.c_pending}`}>
                                            {element.verified === true ? 'Verified': 'Pending'}
                                        </span>
                                        <Link 
                                            to={'/seller-account'}
                                            state={element.id}
                                        >
                                            <FeatherIcon className={style.down_icon} icon="chevron-right"/>
                                        </Link>
                                    </div>
                                </td>
                            </tr>
                        )) }
                        
                    </tbody>
                </table>
                : <Loader />}
            </div> 
        </>
    )
}

export default Commission;
