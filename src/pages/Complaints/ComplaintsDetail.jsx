import React, { useState, useEffect } from 'react'
import style from "./ComplaintsDetail.module.css";
import moment from 'moment';
import {useDispatch, useSelector} from 'react-redux';
import { useParams } from 'react-router-dom';
import { postComplaintResponse, updateComplaintToClose } from '../../store/reducer/complaintReducer';
import { setError } from '../../store/reducer/errorReducer';

const ComplaintsDetail = () => {
    const { complaints, isResponseLoading, isError, error } = useSelector((state) => state.complaintSlice);
    const params = useParams();
    const {id} = params;
    const [complaint, setComplaint] = useState({});
    const [response, setResponse] = useState('');
    const dispatch = useDispatch();
    const handleClickToClose = (id) => {
        if(id !== undefined){
            dispatch(updateComplaintToClose({id:id}));
        }
    }
    const handleClick = (id) => {
        if(response !== '' && id !== undefined){
            dispatch(postComplaintResponse({complaint_id: id, new_response: response}));
            setResponse('');
        }
    } 
    console.log("complaints: ", complaints, `isResponseLoading: `, isResponseLoading);
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(() => {
        setComplaint(complaints?.find(item => item.id === id));
    }, [complaints, id]);
    return (
        <>
            <div>
                <table className={style.cd_table}>
                    <tbody>
                        <tr className={` ${style.cd_row}  ${style.cd_pt}`}>
                            <td className={style.cd_box}>
                                <div className={style.cd_row}>
                                    <div className={`${style.cd_th}`}>Customer Name:</div>
                                    <div className={style.cd_td}>{complaint.name}</div>
                                </div>
                                <div className={` ${style.cd_row}  ${style.cd_pt}`}>
                                    <div className={`${style.cd_th}`}>Subject:</div>
                                    <div className={style.cd_td}>{complaint.subject}</div>
                                </div>
                                
                                <div className={`${style.cd_th} ${style.cd_pt}`}>Description:</div>
                                
                                <div className={`${style.cd_td} ${style.cd_description_box}`}>
                                    {complaint.description}
                                </div>
                            
                            </td>
                            {(complaint.status === true) && (
                                <td className={style.cd_box_2}>
                                    <div className={style.cd_row_custom}>
                                        <div className={`${style.cd_td} ${style.cd_r} ${style.cd_td_light}`}>Change to  --&gt;</div>
                                        <div className={style.cd_td}>
                                            <button className={`${style.cd_button} ${style.submit_btn}`} onClick={()=>handleClickToClose(complaint.id)}>Closed</button>
                                        </div>
                                    </div>
                                    <div className={style.replyCard}>
                                        <div className={`${style.cd_th}`}>Reply:</div>
                                        <div className={style.cd_pt}>
                                            <div className={style.cd_input_box}>
                                                <input type="text" className={style.cd_input} value={response} onChange={ (e) => setResponse(e.target.value)} placeholder='Write a response' />
                                            </div> 
                                        </div>
                                        <div style={{textAlign:'center', paddingTop: '5px'}}>
                                            <button className={`${style.cd_button} ${style.submit_btn}`} onClick={()=>handleClick(complaint.id)}>{!isResponseLoading ? 'Submit' : '...'}</button>
                                        </div>
                                    </div>
                                </td>
                            )}
                        </tr>
                        <tr>
                            <td colSpan={"2"} className={style.cd_p_10}>
                                <div className={`${style.cd_th}`}>{complaint.responses !== undefined ? 'Responses:' : null}</div>
                                {complaint.responses !== undefined && complaint?.responses.map((element, index)=>(
                                    <div className={`${style.cd_row} ${style.cd_pt}`} key={index}>
                                        <div className={style.cd_w}>
                                            <div className={`${style.cd_td} ${style.cd_text_underline}`}>{element.from !== undefined ? `${element.from}` : complaint.name}</div>
                                            <div className={`${style.cd_td} ${style.cd_text_underline}`}>({moment(element.datetime).format('DD/MM/YYYY, h:mm A')})</div>  
                                        </div>
                                        <div className={`${style.cd_td} ${style.cd_italic}`}>
                                            {element.response}
                                        </div>
                                    </div>   
                                ))}
                                                    
                            </td>
                        </tr>  
                    </tbody>
                </table>
            </div>
        </>
    )
}

export default ComplaintsDetail;
