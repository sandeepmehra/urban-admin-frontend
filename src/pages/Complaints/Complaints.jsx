import React, {useState, useEffect} from 'react'
import style from "./Complaints.module.css";
import { useSelector, useDispatch } from 'react-redux';
import { Accordion } from '../../components/Accordion';
import { Outlet } from 'react-router-dom';
import { Loader } from '../../components/Loader/Loader';
import { resComplaintData } from '../../store/reducer/complaintReducer';
import { setError } from '../../store/reducer/errorReducer';

const Complaints = () => {
    const { complaints, isLoading, isError, error } = useSelector((state) => state.complaintSlice);
    const dispatch = useDispatch();
    const [isOpen, setIsOpen] = useState([]);
    
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(()=>{
        dispatch(resComplaintData());
    }, [dispatch]);
    return (
        <>
            <div className={style.main}>
                <h1 className={style.customer_heading}>Complaints</h1>
                {!isLoading ? 
                    <table className={style.table}>
                        <thead className={style.thead}>
                            <tr className={style.complaints_tr}>
                                <th className={style.c_th}>Complaint no.</th>
                                <th className={style.c_th}>Subject</th>
                                <th className={style.c_th}>Status</th>
                                <th className={style.c_th}></th>
                            </tr>
                        </thead>
                        {complaints && complaints.map((element, index)=>(<tbody className={style.tbody}  key={index}>
                            <tr className={style.complaints_tr}>
                                <td className={style.td}>{element.id}</td>
                                <td className={style.td}>{element.subject}</td>
                                <td className={style.td}>{element.status === true ? 'Open': 'Close'}</td>
                                <td className={style.td}>
                                <div className={style.status_costumer}>
                                    <Accordion setIsOpen={setIsOpen} isOpen={isOpen} id={element.id}/>
                                    </div> 
                                </td>
                            </tr>
                            { isOpen.includes(element.id) && ( 
                                <tr className={style.detail_shown}>
                                    <td colSpan="5">
                                        <Outlet /> 
                                    </td>
                                </tr>
                                )
                            }      
                        </tbody>))}
                    </table>
                :<Loader />}
            </div>    
        </>
    )
}

export default Complaints;
