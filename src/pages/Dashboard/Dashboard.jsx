import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import style from "./Dashboard.module.css";
import FeatherIcon from 'feather-icons-react';
import { CommissionBox } from '../../components/Boxes/CommissionBox';
import { Loader } from '../../components/Loader/Loader';
import { Link } from 'react-router-dom';
import { resHomeData } from '../../store/reducer/homeReducer';
import { setError } from '../../store/reducer/errorReducer';


const Dashboard = () => {
    const { complaints, current_commission, inventory, verification, notifications, orders, isLoading, isError, error } = useSelector((state) => state.homeSlice);
    console.log("isLoading in home: ", isLoading, verification, orders, complaints);
    const dispatch = useDispatch();
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(()=>{
            dispatch(resHomeData())
    }, [dispatch]);
    return (
        <>
            <section className={style.main}>
                {!isLoading ? 
                <>
                    <div className={style.container}>
                        <div className={style.card}>
                            <div className={style.card_item}>
                                <h2>Verification</h2>
                                    <div className={style.card_status}>
                                        <div className={style.heading}>
                                            <div>Pending</div> <div className={style.status}>{verification !== undefined && verification.pending !== undefined ? verification.pending : 'NA' }</div>
                                        </div>
                                        <div className={style.heading}>
                                            <div>Approved</div> <div className={style.status}>{verification !== undefined && verification.approved !== undefined ? verification.approved : 'NA'}</div>
                                        </div>
                                    </div>      
                            </div>
                            <div className={style.view_more}>
                                <Link to={"/seller-account"}>
                                    <span className={style.view_more_a}>View more</span>
                                </Link>
                            </div>
                        </div>
                        <div className={style.card}>
                            <div className={style.card_item}>
                                <h2>Inventory</h2>
                                    <div className={style.card_status}>
                                        <div className={style.heading}>
                                            <div>Pending</div> <div className={style.status}>{inventory !== undefined && inventory.pending !== undefined ? inventory.pending : 'NA'}</div>
                                        </div>
                                        <div className={style.heading}>
                                            <div>Approved</div> <div className={style.status}>{inventory !== undefined && inventory.approved !== undefined ? inventory.approved : 'NA'}</div>
                                        </div>
                                    </div>      
                            </div>
                            <div className={style.view_more}>
                                <Link to={"/inventory"}>
                                    <span className={style.view_more_a}>View more</span>
                                </Link>
                            </div>
                        </div>
                        <div className={style.card}>
                            <div className={style.card_item}>
                                <h2>Complaints</h2>
                                    <div className={style.card_status}>
                                        <div className={style.heading}>
                                            <div>Opened</div> <div className={style.status}>{complaints !== undefined && complaints.opened !== undefined ? complaints.opened : 'NA'}</div>
                                        </div>
                                        <div className={style.heading}>
                                            <div>Closed</div> <div className={style.status}>{complaints !== undefined && complaints.closed !== undefined ? complaints.closed : 'NA'}</div>
                                        </div>
                                    </div>      
                            </div>
                            <div className={style.view_more}>
                                <Link to={"/complaints"}>
                                    <span className={style.view_more_a}>View more</span>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className={style.container}>
                        <div className={style.card}>
                            <div className={style.card_item}>
                                <h2>Notifications</h2>
                                {notifications ? notifications.map((item, i)=>(
                                    <div className={style.card_messages} key={i}>
                                    <div className={style.message_circle}>
                                        <FeatherIcon icon="mail" stroke="white" size={20} strokeWidth={2} />
                                        </div>
                                        <div className={style.card_info}>
                                            <h5 className={style.h5}>{item.title}</h5>
                                            <p className={style.dashboard_p_date}>{item.duration}</p>
                                        </div>
                                    </div>)) : null  }   
                            </div>
                            <div className={style.view_more}>
                                <Link to={"/notifications"}>
                                    <span className={style.view_more_a}>View more</span>
                                </Link>
                            </div>
                        </div>
                        
                        <div className={style.break_two_card}>
                            <CommissionBox current_commission={current_commission}/>
                            <div className={style.card}>
                                <div className={style.card_item}>
                                    <h2>Payments</h2>
                                        <div className={style.card_status}>
                                            <div className={style.heading}>
                                                <div>Pending</div> <div className={style.status}>XXXX</div>
                                            </div>
                                            <div className={style.heading}>
                                                <div>Settled</div> <div className={style.status}>XXXX</div>
                                            </div>
                                        </div>      
                                </div>
                                <div className={style.view_more}>
                                    <span className={style.view_more_a}>View more</span>
                                </div>
                            </div>
                        </div>
                        <div className={style.card}>
                            <div className={style.card_item_order}>
                                <h2>Orders</h2>
                                <div className={style.card_status}>
                                    {orders !== undefined && orders ? orders.map((item) => (
                                        <div className={style.heading} key={item.id}>
                                            <div className={style.p10}>{item.id}</div> <div className={` ${style.status} ${style.p10}`}>{item.status}</div>
                                        </div>))
                                        :
                                        <>
                                        <div className={`${style.heading} ${style.card_order}`} key={100008}>
                                            <div className={style.p10}>{'100008'}</div> <div className={` ${style.status} ${style.p10}`}>{'shipped'}</div>
                                        </div>
                                        <div className={`${style.heading} ${style.card_order}`} key={100006}>
                                            <div className={style.p10}>{'100006'}</div> <div className={` ${style.status} ${style.p10}`}>{'Pending'}</div>
                                        </div>
                                        <div className={`${style.heading} ${style.card_order}`} key={100003}>
                                            <div className={style.p10}>{'100003'}</div> <div className={` ${style.status} ${style.p10}`}>{'Pending'}</div>
                                        </div>
                                        <div className={`${style.heading} ${style.card_order}`} key={100002}>
                                            <div className={style.p10}>{'100002'}</div> <div className={` ${style.status} ${style.p10}`}>{'Accepted'}</div>
                                        </div>
                                        <div className={`${style.heading} ${style.card_order}`} key={100001}>
                                            <div className={style.p10}>{'100001'}</div> <div className={` ${style.status} ${style.p10}`}>{'Delivered'}</div>
                                        </div>    
                                        </> 
                                    }
                                </div>      
                            </div>
                            <div className={style.view_more_order}>
                                <Link to={"/orders"}>
                                    <span className={style.view_more_a}>View more</span>
                                </Link>
                            </div>
                        </div>
                    </div>
                </>  : <Loader />}
            </section>
        </>
    )
}

export default Dashboard;
