import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import style from "./Notification.module.css";
import FeatherIcon from 'feather-icons-react';
import { Accordion } from '../../components/Accordion';
import { Outlet } from 'react-router-dom';
import { Loader } from '../../components/Loader/Loader';
import { resNotificationData } from '../../store/reducer/notificationReducer';
import { setError } from '../../store/reducer/errorReducer';


const Notification = () => {
    const { notifications, isLoading, isError, error } = useSelector((state) => state.notificationSlice);
    const dispatch = useDispatch();
    const [isOpen, setIsOpen] = useState([]);
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(()=>{
        dispatch(resNotificationData());
    }, [dispatch]);
    return (
        <>
            <div className={style.main}>
                <h1 className={style.customer_heading}>Notifications</h1>
                {!isLoading ? 
                    <section>
                        {notifications !== undefined ? notifications.map((element, index) => (
                            <div className={style.relative} key={element.id}>
                                <div className={style.n_row}>
                                <div className={style.n_inner_row}>
                                    <div className={style.n_messages}>
                                        <div className={style.n_message_circle}>
                                        <FeatherIcon icon="mail" stroke="white" size={20} strokeWidth={2} />
                                        </div>
                                    </div>
                                    <p className={style.n_p}>{element.title}</p>   
                                </div>
                        
                                <div className={style.n_inner_row}>
                                    <small className={style.n_date}>
                                        {element.duration}
                                    </small>
                                    <div className={style.n_icon_btn}>
                                        <Accordion setIsOpen={setIsOpen} isOpen={isOpen} id={element.id}/>
                                    </div> 
                                </div>
                            </div>  
                            {isOpen.includes(element.id) && ( <Outlet />)}
                            </div>
                            )) 
                            : null
                        }    
                    </section>
                : <Loader />}
            </div>
        </>
    )
}

export default Notification;
