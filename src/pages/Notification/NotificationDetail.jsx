import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { setError } from '../../store/reducer/errorReducer';
import style from"./NotificationDetail.module.css";

const NotificationDetail = () => {
    const { notifications, isError, error } = useSelector((state) => state.notificationSlice);
    const dispatch = useDispatch();
    const params = useParams();
    const {id} = params;
    const [notification, setNotification] = useState({});
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(() => {
        setNotification(notifications?.find(item => item.id === id));
    }, [notifications, id]);  
    return (
        <>
        <div>
            <div className={style.nd_card}>
                <p className={style.nd_p}>
                   {notification.message}
                </p>
            </div>
        </div>
        </>
    )
}

export default NotificationDetail;
