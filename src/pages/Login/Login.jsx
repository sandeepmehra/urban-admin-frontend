import { useForm } from "react-hook-form";
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useSelector, useDispatch} from 'react-redux';
import style from './Login.module.css';
import logo from '../../logo.PNG';
import illustration from '../../illustration.svg';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import { useLocation, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { userLoginByEmail } from "../../store/reducer/authReducer";

const requiredEmailError = 'Email is required';
const InvalidFormat = 'Invalid email format';
const requiredPwdError = 'Password is required';
const pwdMin = 'Password should be 8 character.';
let validationSchema = yup.object().shape({
    email: yup.string().required(requiredEmailError).email(InvalidFormat),
    password: yup.string().required(requiredPwdError).min(5, pwdMin),
});


const Login = () => {
    let navigate = useNavigate();
    let location = useLocation();
    let from = location.state?.from?.pathname || "/";
    const {isLoading, name} = useSelector((state) => state.auth);
    const dispatch = useDispatch();
    const { 
        register, 
        handleSubmit,  
        formState: { errors, isDirty, isValid } 
    } = useForm({
        resolver: yupResolver(validationSchema),
        mode: "onChange",
        defaultValues: {
            email: "",
            password: "",
          }
      });

    const onSubmit = (data) => {
       dispatch(userLoginByEmail(data));
    }
    useEffect(() => {
        if (name) {
            navigate(from, { replace: true });
        }
      }, [navigate, name, from]);
    return (
        <>
        
            <div className={style.container}>
                <div className={style.col_first}>
               
                    <div className={style.login_card}>
                        <img 
                            src={logo} 
                            className={style.app_logo}
                            alt='Logo' 
                        />
                        <h1 className={style.card_heading}>Login</h1>
                        {/* <div className={`${style.ErrorMessage} ${style.MainError}`}>{loginErrors.message ? <div>{loginErrors.message}</div>: null}</div> */}
                        <form onSubmit={handleSubmit(onSubmit)} className={style.formstyle}>
                            <div>
                                <Input 
                                    name="email"
                                    type="email"
                                    placeholder="Email Address"
                                    autoComplete='off'
                                    register = {register}
                                    className={style.input_item}
                                    errors={errors}
                                    error_style={style.ErrorMessage}
                                    error_body={style.ErrorBody}
                                />
                                
                            </div>
                            <div>
                                <Input 
                                    name="password"
                                    type="password"
                                    placeholder="Password"
                                    autoComplete='off'
                                    register={register}
                                    errors={errors}
                                    error_style={style.ErrorMessage}
                                    className={style.input_item} 
                                    error_body={style.ErrorBody} 
                                />  
                            </div>
                            
                            <div className={style.center}>
                                <Button 
                                    type="submit" 
                                    className={isValid ? `${style.login_btn}`: `${style.login_btn} ${style.login_btn_disable}`} 
                                    disabled={!isValid && !isDirty}
                                >
                                    {isLoading ? '...' : 'Login'}
                                </Button>
                            </div>
                            
                        </form>
                       
                    </div>   
                </div>
                <div className={style.col_second}>
                    <h1 className={style.card_2_heading}>HI Xyz!</h1>
                    <h2 className={style.card_2_sub_heading}>Welcome Back</h2>
                    <img src={illustration} className={style.illustration} alt='welcome' />
                </div>
            </div>
        </>
    )
}
export default Login;