import React, { useEffect, useState } from 'react';
import style from "./Seller.module.css";
import { useDispatch, useSelector } from 'react-redux';
import SellerChild from './SellerChild';
import { resSellerData } from '../../store/reducer/sellerReducer';
import { setError } from '../../store/reducer/errorReducer';

const Seller = () => {
    const { sellers, isLoading, isError, error } = useSelector((state) => state.sellerSlice);
    const [sortData, setSortData] = useState([]);
    const dispatch = useDispatch();
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(()=>{
        dispatch(resSellerData());
    },[dispatch]);
    useEffect(()=>{
        if(sellers !== undefined && sellers.length !== 0) {
            setSortData(sellers);
        }
    },[sellers, sellers.length]);
    return (
        <>
            <div className={style.main}>
                <SellerChild sellers={sellers} loading={isLoading} sortData={sortData} setSortData={setSortData}/>
            </div>
        </>
    )
}

export default Seller;
