import React, {useEffect, useState} from 'react';
import { Outlet, useLocation } from 'react-router-dom';
import style from "./Seller.module.css";
import Dropdown from '../../components/Dropdown/Dropdown';
import { Accordion } from '../../components/Accordion';
import { Loader } from '../../components/Loader/Loader';

const SellerChild = ({sellers, loading, sortData,setSortData}) => {
    
    const [isOpen, setIsOpen] = useState([]);
    const [defaultOpen, setDefaultOpen] = useState('');
    const location = useLocation();
    const [option, setOption] = useState({
        value: "Date",
        label: "Date",
        key: 0
    })
    const options = [
        {
            value: "Date",
            label: "Date",
            key: 0
        },
        {
            value : "NumberOfProducts",
            label : "Number of products",
            key: 1
        },
        {
            value : "Pending",
            label : "Verification Pending",
            key: 2
        }
    ];
    useEffect(() => {
        if(location !== undefined && location.state !== null) {
            setIsOpen(location.state);
            setDefaultOpen(location.state);
        }
    }, [location]);
    const chooseOption = (value) => {
        setOption({
            ...option,
            value: value.value,
            label: value.label,
            key: value.key
        });
        switch(value.value){
            case 'Date':
                const sortedByDate = sortData.slice().sort((a, b)=>(new Date(a.created_at) > new Date(b.created_at)) ? -1 : 1);
                setSortData(sortedByDate);
                break;
            case 'NumberOfProducts':
                const sortedByProducts = sortData.slice().sort((a, b)=> (a.shop.numberOfProducts > b.shop.numberOfProducts) ? -1 : 1);
                setSortData(sortedByProducts);
                break;
            case 'Pending':
                let statusOrder = ["Pending", "Approved", "Rejected"];
                const sortedByPending = sortData.slice().sort((a, b) => statusOrder.indexOf(a.status) - statusOrder.indexOf(b.status));
                setSortData(sortedByPending);
                break;
            default:
                setSortData(sortData);
        }
    }  
    return (
        <>
            <span className={style.heading}>Seller Account</span>
            <span className={style.seller_filter}>Sort by:</span>
            <div className={style.filter_box}>
                <Dropdown 
                    menuHeight={100}
                    optionsData={options}
                    chooseUnitData={chooseOption}
                    placeholder={option.value}
                    defaultValue={options[0]}
                />
            </div>
            {!loading ? 
                <table className={style.table}>
                    <thead>
                        <tr className={style.tr}>
                            <th className={style.th}>Seller Name</th>
                            <th className={style.th}>Shop Name</th>
                            <th className={style.th}>Whatsapp number</th>
                            <th className={style.th}>Address</th>
                            <th className={style.th}>Verification Status</th>
                        </tr>
                    </thead>

                    { sortData.map((element, index)=>(<tbody key={index} className={style.tbody}>
                        <tr className={style.tr}>
                            <td className={style.td}>{element.seller_name}</td>
                            <td className={style.td}>{element.shop.name}</td>
                            <td className={style.td}>+91 {element.shop.wa_number}</td>
                            <td className={style.td}>{element.shop.city}, {element.shop.state}</td>
                            <td className={style.td}>
                                <div className={style.status}>
                                    <span className={`${element.status==='Approved'? `${style.btn} ${style.approve}` : style.btn}`}>
                                        {element.status}
                                    </span>
                                    <span>
                                        {
                                            defaultOpen === element.id ? 
                                                <Accordion setIsOpen={setIsOpen} isOpen={isOpen} id={defaultOpen} value={true} defaultOpen={defaultOpen} setDefaultOpen={setDefaultOpen} /> 
                                                : 
                                                <Accordion setIsOpen={setIsOpen} isOpen={isOpen} id={element.id}/>
                                        }
                                    </span>
                                </div>
                            </td>
                        </tr>
                        {isOpen.includes(element.id) && (  
                            <tr>
                                <td colSpan="5">
                                    <Outlet />
                                </td>
                            </tr>)
                        }
                    </tbody>))}
                </table>
            : <Loader />}
        </>
    )
}

export default SellerChild;
