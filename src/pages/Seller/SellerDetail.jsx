import React, { useEffect, useState } from 'react';
import style from "./SellerDetail.module.css";
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { diff } from 'deep-object-diff';
import { updateSellerInfo, updateSellerStatus } from '../../store/reducer/sellerReducer';
import { toast } from 'react-toastify';
import { setError } from '../../store/reducer/errorReducer';

const SellerDetail = () => {
    const {  sellers, isError, error } = useSelector((state) => state.sellerSlice); 
    const params = useParams();
    const {id} = params;
    const [currentData, setCurrentData] = useState({
        seller_name: '',
        email: '',
        mobile_number: ''
    });
    const [seller, setSeller] = useState({});
    const dispatch = useDispatch();
    const inputHandler = (e) => {
        setCurrentData({
            ...currentData,
            [e.target.name] : e.target.value, 
        });
    }
    const handleApproveClick = (id) => {
        if(id !== undefined){
            dispatch(updateSellerStatus({id:id, updateValue:'approve'}));
        } 
    }
    const handleRejectClick = (id) => {
        if(id !== undefined){
            dispatch(updateSellerStatus({id:id, updateValue:'reject'}));
        }
    }
    const handleUpdateClick = (id, updatedDetail) => {
        if(Object.entries(updatedDetail).length !== 0){
            dispatch(updateSellerInfo({id:id, updateValue: updatedDetail}));

        } else {
            toast.info('Nothing to update');
        }
    }
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(() => {
        setSeller(sellers?.find(item => item.id === id));
        setCurrentData(seller);
    }, [sellers, id, seller]); 
    
    return (
        <>
            <div>
            <table className={style.table}>
                <tbody>
                <tr className={style.row}>
                    <th colSpan="5" className={`${style.th} ${style.padding}`}>User Information</th>
                </tr>
                <tr className={style.row_bg}>
                    <td>
                        <table className={style.user_table}>
                            <tbody>
                                <tr> 
                                    <th className={style.user_th}>
                                        Name: 
                                            <input 
                                                type={'text'} 
                                                defaultValue={seller?.seller_name} 
                                                name={"seller_name"}
                                                onChange={inputHandler} 
                                                className={style.user_input} 
                                            /> 
                                    </th>
                                    <th className={style.user_th} colSpan={2}>
                                        Commission set: <h3 style={{display: 'inline-block'}}>{seller?.commission_percentage}%</h3>  
                                    </th>
                                    <th className={style.user_th}>
                                        <button className={style.user_button} onClick={()=>handleUpdateClick(seller?.id, diff(seller, currentData))}>Update</button>
                                   </th>
                                </tr>
                               <tr>
                                    <th className={style.user_th}>
                                        Email: 
                                        <input 
                                            type={'email'} 
                                            defaultValue={seller?.email} 
                                            className={style.user_input}
                                            name={"email"}
                                            onChange={inputHandler}  
                                            style={{marginLeft: '2px'}} 
                                        /> 
                                    </th>
                                    <th className={style.user_th}>
                                        Phone Number: 
                                        <input 
                                            type={'text'} 
                                            defaultValue={seller?.mobile_number}
                                            name={"mobile_number"} 
                                            className={style.user_input}
                                            onChange={inputHandler} 
                                        />                        
                                    </th>
                               </tr>        
                            </tbody>
                        </table>
                    </td>
                </tr>
               
                <tr>
                    <th colSpan="5" className={`${style.th} ${style.padding}`}>Shop Information</th>
                </tr>
                <tr className={style.row_bg}>
                    <td>
                        <table className={style.shop_info_table}>
                            <tbody>
                                
                                <tr>
                                    <th className={style.shop_info_th}>
                                        <div className={style.shop_info_logo}>
                                            Shop Logo: <img src={`${seller?.shop?.logo}`} className={style.seller_logo} alt={seller?.shop?.name} />
                                        </div>
                                    </th>
                                    <th className={style.shop_info_th}>
                                        Shop Name: <span className={style.p_style}>{seller?.shop?.name}</span>
                                    </th>
                                    <th colSpan="3" className={style.shop_info_th}>
                                        <div className={style.shop_info_box}>
                                            <div>Shop Category:</div>
                                            <p className={style.p_style}>Pots and Planters</p>
                                        </div>                            
                                    </th>  
                                </tr>
                                <tr>
                                    <th rowSpan={"3"} className={style.shop_info_th}>
                                        <div className={style.shop_info_description}>
                                            Shop Description:
                                            <p className={style.shop_info_description_p}>{seller?.shop?.description}</p>
                                        </div>                       
                                    </th>
                                    <th className={style.shop_info_th}>
                                        Shop url:
                                        <span className={style.p_style}>{seller?.shop?.url}</span>                 
                                    </th>
                                </tr>
                                <tr>
                                    <th className={style.shop_info_th}>
                                        <div className={style.card_style}>
                                        <div style={{width:"120px"}}>Shop Contact Number: </div>
                                        <div className={style.card}>
                                            <span className={style.card_style_p}>phone - +91 {seller?.shop?.mobile_number},</span>
                                            <span className={style.card_style_p}>Whatsapp - +91 {seller?.shop?.wa_number} </span>
                                        </div>
                                        </div>
                                    </th>
                                
                                </tr>
                                <tr>
                                    <th className={style.shop_info_th}> 
                                        <div className={style.card_style}>  
                                        <div style={{width:"120px"}}>Shop Address: </div>
                                        <div className={style.card}>
                                            <span className={style.card_style_p}>{seller?.shop?.street}, {seller?.shop?.city},{seller?.shop?.state}, {seller?.shop?.pincode} </span>
                                        </div>
                                        </div>    
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
               
                <tr>
                    <th colSpan="5" className={`${style.th} ${style.padding}`}>Verification Documents</th>
                </tr>
                <tr className={style.row_bg}>
                    <td>
                        <table className={style.document_table}>
                            <tbody>
                            <tr>
                                <th className={style.document_th}>Adhar Card</th>
                                <td className={style.document_td}><span className={style.document_preview} onClick={()=> window.open(seller?.verification?.aadhar_card, "_blank")}>Preview</span></td>
                                <td rowSpan="4" style={{"width": "30%"}}>
                                {seller?.verification?.action_taken !== 'true' && (
                                    <div className={style.document_buttons}>
                                        <button className={style.document_button} onClick={()=>handleApproveClick(seller.id)}>Verify</button>
                                        <button className={style.document_button} onClick={()=>handleRejectClick(seller.id)}>Reject</button>
                                    </div>
                                )}
                                </td>
                            </tr>
                            <tr>
                                <th className={style.document_th}>Pan Card</th>
                                <td className={style.document_td}><span className={style.document_preview} onClick={()=> window.open(seller?.verification?.pan_card, "_blank")}>Preview</span></td>
                            </tr>
                            <tr>
                                <th className={style.document_th}>Selfie</th>
                                <td className={style.document_td}><span className={style.document_preview} onClick={()=> window.open(seller?.verification?.selfie, "_blank")}>Preview</span></td>
                            </tr>
                            <tr>
                                <th className={style.document_th}>Bank passbook/cancelled cheque/bank statement</th>
                                <td className={style.document_td}><span className={style.document_preview} onClick={()=> window.open(seller?.verification?.bank_document, "_blank")}>Preview</span></td>
                                <td rowSpan="6"></td>
                            </tr>
                            <tr>
                                <th className={style.document_th}>GST Number</th>
                                <td className={style.document_td}>{seller?.verification?.gst_number}</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
               
                </tbody>
            </table>
            </div>
        </>
    )
}

export default SellerDetail;
