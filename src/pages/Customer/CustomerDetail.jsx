import React, {useEffect, useState} from 'react';
import style from "./Detail.module.css";
import { CustomerReview } from './CustomerReview';
import { useDispatch, useSelector } from 'react-redux';
import { ShowStarRating } from '../../components/StarRating/ShowStarRating';
import { useParams } from 'react-router-dom';
import { putCustomerData } from '../../store/reducer/customerReducer';
import { toast } from 'react-toastify';
import { setError } from '../../store/reducer/errorReducer';



const CustomerDetail = () => { 
    const {  customers, isError, error } = useSelector((state) => state.customerSlice);
    console.log("CustomersDetail: ", customers);
    const params = useParams();
    const {id} = params;
    const [customer, setCustomer] = useState({});
    const dispatch = useDispatch();
    const canSuspend = (id) => {
        if(customer.suspended === true){
            toast.info('Already suspended');
        } else {
            dispatch(putCustomerData({id: id, updateValue: true}));
        }
    }  
    const canActivate = (id) => {
        if(customer.suspended === false){
            toast.info('Already active');
        } else {
            dispatch(putCustomerData({id: id, updateValue: false}));
        }
    }   
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]); 
    useEffect(() => {
        setCustomer(customers?.find(item => item.id === id));
    }, [customers, id]);                  
    return (
        <>
           <div className={style.main_card}>
                <table className={`${style.table} ${style.scroller}`}>
                    <thead>
                        <tr>
                            <th className={`${style.th} ${style.th_s_a}`}>Product Name</th>
                            {customer.order_items !== undefined &&<th className={style.th}>SKU</th>}
                            {customer.order_items !== undefined &&<th className={style.th}>Rating</th>}
                        </tr>
                    </thead>
                    <tbody>
                        {customer.order_items !== undefined ? customer.order_items.map((item, index)=>(
                            <tr className={style.row} key={index}>
                                <td className={`${style.td} ${style.td_s} ${style.td_s_a}`}>{item.name}</td>
                                <td className={`${style.td} ${style.td_s}`}>{item.sku}</td>
                                <td className={`${style.td} ${style.td_s}`}>
                                    <ShowStarRating value={item.rating} />                                       
                                </td>        
                            </tr>
                            ))
                            :
                            <tr>
                                <td className={`${style.td_not} ${style.td_s_a}`} colSpan="6">
                                    No product order till now
                                </td>
                            </tr>
                            
                        }
                    </tbody>
                </table>
                <div className={style.customer_btn}>
                    <button 
                        className={style.suspend_btn} 
                        onClick={()=>canSuspend(customer.id)}>
                            Suspend
                    </button>
                    <button 
                        className={style.activate_btn} 
                        onClick={()=>canActivate(customer.id)}>
                            Activate
                    </button> 
                </div>
                {customer && <CustomerReview customer={customers?.find(item => item.id === id)}/>}
            </div>
        </>
    )
}

export default CustomerDetail;
