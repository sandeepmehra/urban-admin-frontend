import React, { useEffect } from 'react';
import style from './Detail.module.css';
import { useDispatch, useSelector } from 'react-redux';
import { putReviewData, resReviewData } from '../../store/reducer/customerReducer';
import { Loader } from '../../components/Loader/Loader';
import { setError } from '../../store/reducer/errorReducer';

export const CustomerReview = ({customer}) => {
  const {  reviews, isReviewLoading, isError, error } = useSelector((state) => state.customerSlice);
  console.log("reviews: ", reviews, customer);
  const dispatch = useDispatch();
  const canApproved = async (review_id, id) => {
    console.log("review_id, id", review_id, id);
    if(review_id !== undefined && id !== undefined){
        dispatch(putReviewData({review_id: review_id, approved: true, id: id}));
    }   
  }
  const canReject = (review_id, id) => {
    if(review_id !== undefined && id !== undefined){
      dispatch(putReviewData({review_id: review_id, approved: false, id: id}));
    }
  }
  useEffect(()=>{
    dispatch(setError(isError ? error : null));
}, [isError, error, dispatch]);
  useEffect(()=>{
      dispatch(resReviewData(customer.id));
  }, [dispatch, customer.id]);
  return (
    <div>
      {!isReviewLoading ? 
        <div className={style.review}>
            <h1 className={style.review_heading}>Review</h1>
            <p className={style.review_p}>{reviews.review !== undefined ? reviews.review : 'No review pending'}</p>
            {(Object.keys(reviews).length !== 0) && <div>
                <button 
                  className={style.approve_btn} 
                  onClick={()=>canApproved(reviews.id, customer.id)}>
                    Approve
                </button>
                <button 
                  className={style.reject_btn} 
                  onClick={()=>canReject(reviews.id, customer.id)}>
                    Reject
                </button>
            </div>}
        </div> : <Loader />}
    </div>
  )
}
