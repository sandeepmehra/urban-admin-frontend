import React, { useState, useEffect } from 'react';
import style from "./Customer.module.css";
import { useDispatch, useSelector } from 'react-redux';
import { Outlet } from 'react-router-dom';
import { Accordion } from '../../components/Accordion';
import { Loader } from '../../components/Loader/Loader';
import { resCustomerData } from '../../store/reducer/customerReducer';
import { setError } from '../../store/reducer/errorReducer';

const Customer = () => {
    const {  customers, isLoading, isError, error } = useSelector((state) => state.customerSlice);
    const [isOpen, setIsOpen] = useState([]);
    const dispatch = useDispatch();
    useEffect(()=>{
        dispatch(setError(isError ? error : null));
    }, [isError, error, dispatch]);
    useEffect(()=>{
        dispatch(resCustomerData())
    }, [dispatch]);
    return (
        <>
            <div className={style.main}>
                <h1 className={style.heading}>Customer Account</h1>
                {!isLoading ? 
                <table className={style.table}>
                    <thead className={style.thead}>
                        <tr className={style.tr}>
                            <th className={style.th}>Name</th>
                            <th className={style.th}>Email Address</th>
                            <th className={style.th}>Mobile number</th>
                            <th className={style.th}>Address</th>
                            <th className={style.th}>Status</th>
                        </tr>
                    </thead>
                    {customers !== undefined && customers.map((element, index)=>(<tbody className={style.tbody} key={index}>
                            <tr className={style.tr} >
                                <td className={style.td}>{element.name}</td>
                                <td className={style.td}>{element.email}</td>
                                <td className={style.td}>+91 {element.mobileNumber}</td>
                                <td className={style.td}>{element.location}</td>
                                <td className={style.td}>
                                <div className={style.status_costumer} >
                                        <span className={`${element.suspended === true ? `${style.status_btn_suspend} ${style.status_btn}` : style.status_btn}`}>
                                            {element.suspended === true ? 'Suspend': 'Active'}
                                        </span>
                                        <Accordion setIsOpen={setIsOpen} isOpen={isOpen} id={element.id}/>
                                    </div> 
                                </td>
                            </tr>
                            {
                               isOpen.includes(element.id) && ( 
                                <tr className={style.detail_shown}>
                                    <td colSpan="6">
                                        <Outlet />
                                    </td>
                                </tr>
                            )}  
                    </tbody>))}
                </table>
                :<Loader />}
            </div>
        </>
    )
}

export default Customer;