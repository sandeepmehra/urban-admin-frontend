export const diff = (oldObj, newObj) => {
    const added = {};
    const updated = {};
    const deleted = {};
    if(JSON.stringify(Object.keys(oldObj)) !== JSON.stringify(Object.keys(newObj))) return 'Please reload the page';

    for (let key in oldObj) {
        if (oldObj[key] !== newObj[key]) {
            if (key === 'images') {
                if (oldObj[key] !== newObj[key] && oldObj[key] !== undefined) {
                    deleted['images'] = oldObj[key].filter(x => !newObj[key].includes(x));
                }
            } else if (key === 'videos') {
                if (oldObj[key] !== newObj[key] && oldObj[key] !== undefined) {
                    deleted['videos'] = oldObj[key].filter(x => !newObj[key].includes(x));
                }
            } else if (key === 'seo_keywords') {
                if (oldObj[key] !== newObj[key] && oldObj[key] !== undefined) {
                    if (newObj[key].filter(x => !oldObj[key].includes(x)).length > 0) {
                        added['seo_keywords'] = newObj[key].filter(x => !oldObj[key].includes(x));
                    }
                    if (oldObj[key].filter(x => !newObj[key].includes(x)).length > 0) {
                        deleted['seo_keywords'] = oldObj[key].filter(x => !newObj[key].includes(x));
                    }
                }
            } else {
                updated[key] = newObj[key];
            }
        }
    }
    return {
        added,
        updated,
        deleted
    };
}