import React from 'react';
import { BrowserRouter} from 'react-router-dom';
import { Provider } from "react-redux";
import Router from './Routes/Router';
import { store } from './store';
import { ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import ErrorBoundary from './pages/Error/ErrorBoundary';

const App = () => {
  return (
      <Provider store={store}>
        <ToastContainer
            draggable={false}
            autoClose={1000}
        />
        <BrowserRouter>
          <ErrorBoundary>
            <Router />
          </ErrorBoundary>
        </BrowserRouter>
      </Provider>
  )
}

export default App;

