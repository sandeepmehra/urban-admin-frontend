import React from 'react';
import {useSelector} from 'react-redux';
import {Navigate, useLocation} from 'react-router-dom';

export const PrivateRoute = ({children}) => {
    const location = useLocation();
    const { isAuthenticated } = useSelector((state) => state.auth);
  return (
    isAuthenticated ? children : <Navigate to="/login" state={{ from: location }} replace  />
  )
}
