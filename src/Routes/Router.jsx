import { Routes, Route, useLocation } from "react-router-dom";
import Login from '../pages/Login/Login';
import Dashboard from '../pages/Dashboard/Dashboard';
import Customer from '../pages/Customer/Customer'
import Seller from '../pages/Seller/Seller';
import Order from '../pages/Order/Order';
import Inventory from '../pages/Inventory/Inventory';
import Notification from '../pages/Notification/Notification';
import Complaints from '../pages/Complaints/Complaints';
import Commission from '../pages/Commission/Commission';
import NotFound from '../pages/NotFound/NotFound';
import Sidebar from "../components/Sidebar/Sidebar";
import Header from "../components/Header/Header";
import { PrivateRoute } from "./private/PrivateRoute";
import CustomerDetail from "../pages/Customer/CustomerDetail";
import ComplaintsDetail from "../pages/Complaints/ComplaintsDetail";
import NotificationDetail from "../pages/Notification/NotificationDetail";
import Orderdetail from "../pages/Order/Orderdetail";
import SellerDetail from "../pages/Seller/SellerDetail";
import InventoryDetail from "../pages/Inventory/Detail/InventoryDetail";
import { WentWrong } from "../pages/NotFound/WentWrong";

const Router = () => {
    const location = useLocation();
    let showNavigation = location.pathname !== '/login' ? <><Header /><Sidebar /></>: null;
    return (
        <div className="main">
            {showNavigation}
            <Routes> 
                <Route path="/login" element={<Login />} />

                <Route path="/" element={<PrivateRoute><Dashboard /></PrivateRoute>} />
                <Route path="dashboard" element={<PrivateRoute><Dashboard /></PrivateRoute>} />
                <Route path="customer-account" element={<PrivateRoute><Customer /></PrivateRoute>}>
                    <Route path=":id" element={<PrivateRoute><CustomerDetail /></PrivateRoute>} />
                </Route>
                <Route path="seller-account" element={<PrivateRoute><Seller /></PrivateRoute>}>
                    <Route path=":id" element={<PrivateRoute><SellerDetail /></PrivateRoute>} />
                </Route>
                <Route path="orders" element={<PrivateRoute><Order /></PrivateRoute>}>
                    <Route path=":id" element={<PrivateRoute><Orderdetail /></PrivateRoute>} />
                </Route>
                <Route path="inventory" element={<PrivateRoute><Inventory /></PrivateRoute>}>
                    <Route path=":id" element={<PrivateRoute><InventoryDetail /></PrivateRoute>} />
                </Route>
                <Route path="complaints" element={<PrivateRoute><Complaints /></PrivateRoute>}>
                    <Route path=":id" element={<PrivateRoute><ComplaintsDetail /></PrivateRoute>} />
                </Route>
                <Route path="notifications" element={<PrivateRoute><Notification /></PrivateRoute>}>
                    <Route path=":id" element={<PrivateRoute><NotificationDetail /></PrivateRoute>} />
                </Route>
                <Route path="commission" element={<PrivateRoute><Commission /></PrivateRoute>} />

                <Route path="/something-went-wrong" exact element={<WentWrong />} />     
                <Route path="*" element={<NotFound />} />
            </Routes>
        </div>
    )
}

export default Router;
