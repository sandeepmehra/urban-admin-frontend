import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/dist/query';
import authReducer from './reducer/authReducer';
import commissionReducer from './reducer/commissionReducer';
import complaintReducer from './reducer/complaintReducer';
import customerReducer from './reducer/customerReducer';
import errorReducer from './reducer/errorReducer';
import homeReducer from './reducer/homeReducer';
import inventoryReducer from './reducer/inventoryReducer';
import notificationReducer from './reducer/notificationReducer';
import orderReducer from './reducer/orderReducer';
import sellerReducer from './reducer/sellerReducer';

export const store = configureStore({
    reducer: {
        error: errorReducer,
        auth: authReducer,
        homeSlice: homeReducer,
        notificationSlice: notificationReducer,
        commissionSlice: commissionReducer,
        customerSlice: customerReducer,
        sellerSlice: sellerReducer,
        inventorySlice: inventoryReducer,
        complaintSlice: complaintReducer,
        orderSlice: orderReducer
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false,
        }),
});
setupListeners(store.dispatch)
