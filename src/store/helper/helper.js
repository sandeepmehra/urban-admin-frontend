import Cookies from "js-cookie";
import { RESET_TOKEN } from "../Types/authTypes";
import adminApi from '../../api/index';
import jwt_decode from 'jwt-decode';

export const removeAuth = (dispatch)=>{
    if(Cookies.get('at') !== undefined && Cookies.get('rt') !== undefined){
        Cookies.remove('at');
        Cookies.remove('rt');
    }
    dispatch({type: RESET_TOKEN});
}
export const checkAccessTokenExpiry = async () =>{
    const decodedAt = jwt_decode(Cookies.get('at'));
    const expiredAT = decodedAt.exp * 1000 < Date.now();
    if(expiredAT){
        checkAt();     
    }
}
export const config = {
    headers: {
        'Authorization': `Bearer ${Cookies.get('at')}`,
        "Content-Type": "application/json"
    }
}
const checkAt = async() => {
    const {headers: {authorization}} = await adminApi({method: 'post', url:'/refresh', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}, data: {'refresh_token': `${Cookies.get('rt')}`}})
    Cookies.remove('at');
    Cookies.set('at', authorization.split(' ')[1],  { sameSite: 'strict', secure: true });
}