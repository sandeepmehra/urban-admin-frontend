import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import sellerService from '../Services/sellerService';
import { toast } from 'react-toastify';

// Get Seller page data
export const resSellerData = createAsyncThunk(
  'seller/get',
  async (thunkAPI) => {
    try {
        console.log("res seller DAta called");
        return await sellerService.getSellersData();
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)

// Updated Seller Status Approved or Rejected
export const updateSellerStatus = createAsyncThunk(
    'seller/update/approve/reject',
    async ({id, updateValue}, thunkAPI) => {
      try {
        console.log("updateValue, id in updateSellerStatus: ", updateValue, id);
        const response = await sellerService.updateSellerApproveOrRejected(id, updateValue);
        if(response.status !== 204){
            throw Error(`Seller is not ${updateValue}`);
        }
        if(response.status === 204){
          toast.success(`seller has been ${updateValue}.`);
        }
        return await sellerService.getSellersData();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

// Updated Seller Info
export const updateSellerInfo = createAsyncThunk(
  'seller/update/info',
  async ({id, updateValue}, thunkAPI) => {
    try {
      console.log("updateValue, id in updateSellerInfo: ", updateValue, id);
      const response = await sellerService.updateUserInfo(id, updateValue);
      if(response.status !== 204){
          throw Error(`Seller is not ${updateValue}`);
      }
      return await sellerService.getSellersData();
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)
const initialState = {
    sellers: [],
    isError: false,
    isSuccess: false,
    isLoading: true,
    error: null,
}

export const sellerReducer = createSlice({
  name: 'seller',
  initialState,
  reducers: {
    ResetSellerError(state){
      state.error = null;
      state.isError = false;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(resSellerData.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(resSellerData.fulfilled, (state, action) => {
        const {sellers} = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.sellers = sellers;
      })
      .addCase(resSellerData.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.error = action.payload;
      })
      .addCase(updateSellerStatus.fulfilled, (state, action) => {
        const {sellers} = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.sellers = sellers;
      })
      .addCase(updateSellerStatus.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.error = action.payload;
      })
      .addCase(updateSellerInfo.fulfilled, (state, action) => {
        const {sellers} = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        toast.success("seller has been Updated.");
        state.sellers = sellers;
      })
      .addCase(updateSellerInfo.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.error = action.payload;
      })
  },
})

export const { ResetSellerError } = sellerReducer.actions;
export default sellerReducer.reducer;