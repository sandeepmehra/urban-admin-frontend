import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import inventoryService from '../Services/inventoryService';

// Get Inventory page data
export const resInventoryData = createAsyncThunk(
  'inventory/get',
  async (thunkAPI) => {
    try {
        console.log("res inventory DAta called");
        return await inventoryService.getInventoryData();
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)
// Get Unit data
export const resUnitData = createAsyncThunk(
  'unit/get',
  async (thunkAPI) => {
    try {
        console.log("res unit DAta called");
        return await inventoryService.getUnitData();
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)
// Updated Inventory Information 
export const updateInventoryProductInfo = createAsyncThunk(
    'inventory/put',
    async ({id, updateValue}, thunkAPI) => {
      try {
        console.log("updateValue, id in updateInventoryProductInfo: ", updateValue, id);
        const response = await inventoryService.updateInventoryInfo(id, updateValue);;
        if(response.status !== 204){
            throw Error(`Inventory is not ${updateValue}`);
        }
        return await inventoryService.getInventoryData();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)
// Updated Inventory Approved Or Rejected Status 
export const updateInventoryApprovedOrRejected = createAsyncThunk(
    'inventory/approve/reject/put',
    async ({id, value}, thunkAPI) => {
      try {
        console.log("updateInventoryApprovedOrRejected Data: ", id, value);
        const response = await inventoryService.updateApprovedOrRejected(id, value);;
        if(response.status !== 204){
            throw Error(`Review is not ${value}`);
        }
        return await inventoryService.getInventoryData();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)
const initialState = {
    inventory: [],
    units: [],
    isError: false,
    isSuccess: false,
    isLoading: true,
    error: null,
}

export const inventoryReducer = createSlice({
  name: 'inventory',
  initialState,
  reducers: {
    ResetInventoryError(state){
        state.error = null;
        state.isError = false;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(resInventoryData.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(resInventoryData.fulfilled, (state, action) => {
        const {products} = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.inventory = products;
      })
      .addCase(resInventoryData.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.error = action.payload;
      })
      .addCase(resUnitData.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(resUnitData.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.units = action.payload;
      })
      .addCase(resUnitData.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.error = action.payload;
      })
      .addCase(updateInventoryProductInfo.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(updateInventoryProductInfo.fulfilled, (state, action) => {
        const {products} = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        toast.success("Inventory has been updated.");
        state.inventory = products;
      })
      .addCase(updateInventoryProductInfo.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.error = action.payload;
      })
      .addCase(updateInventoryApprovedOrRejected.fulfilled, (state, action) => {
        const {products} = action.payload;
        state.isSuccess = true;
        state.inventory = products;
      })
      .addCase(updateInventoryApprovedOrRejected.rejected, (state, action) => {
        state.isError = true;
        state.error = action.payload;
      })
  },
})

export const { ResetInventoryError } = inventoryReducer.actions;
export default inventoryReducer.reducer;