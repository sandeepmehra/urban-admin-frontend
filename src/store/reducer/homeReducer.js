import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import homeService from '../Services/homeService';
import { toast } from 'react-toastify';

// Get home page data
export const resHomeData = createAsyncThunk(
  'home',
  async (thunkAPI) => {
    try {
        console.log("res home DAta called");
        return await homeService.getHomeData();
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)


// post a Common Commission
export const addCommonCommissionAction = createAsyncThunk(
  'commission/common/add',
  async ({commission_percentage}, thunkAPI) => {
    try {
      console.log("commission_percentage: ", commission_percentage);
      const response = await homeService.addCommission({commission_percentage}); 
      if(response.status ===  204){
          toast.success('Commission updated.');
          return thunkAPI.dispatch(AddCommissionReducer(commission_percentage));
      }
      return;
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)

const initialState = {
    categories: [],
    complaints: {},
    current_commission: '',
    inventory: {},
    verification: {},
    notifications: [],
    orders: [],
    isError: false,
    isSuccess: false,
    isLoading: true,
    error: null,
}

export const homeReducer = createSlice({
  name: 'home',
  initialState,
  reducers: {
    AddCommissionReducer(state, action){
      state.current_commission = action.payload
    },
    ResetHomeError(state){
      state.error = null;
      state.isError = false;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(resHomeData.pending, (state) => {
        state.isLoading = true
      })
      .addCase(resHomeData.fulfilled, (state, action) => {
        const {complaints, current_commission, inventory, verification, notification, orders} = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.complaints = complaints;
        state.current_commission = current_commission;
        state.inventory = inventory;
        state.verification = verification;
        state.notifications = notification;
        state.orders = orders;
      })
      .addCase(resHomeData.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.error = action.payload
      })
  },
})

export const { AddCommissionReducer, ResetHomeError } = homeReducer.actions;
export default homeReducer.reducer;