import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import commissionService from '../Services/commissionService';
import { toast } from 'react-toastify';

// Get Commission page data
export const resCommissionsData = createAsyncThunk(
  'commissions',
  async (thunkAPI) => {
    try {
        console.log("res commissions DAta called");
        return await commissionService.getCommissionData();
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)
// Added Commission by one
export const addCommissionData = createAsyncThunk(
    'commission/add',
    async (value, thunkAPI) => {
      try {
        const response = await commissionService.addCommission(value);;
        if(response.status !== 204){
            throw Error("Commission is not added");
        }
        return await commissionService.getCommissionData();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)

const initialState = {
    commissions: [],
    isError: false,
    isSuccess: false,
    isLoading: true,
    error: null,
}

export const commissionReducer = createSlice({
  name: 'commissions',
  initialState,
  reducers: {
    ResetCommissionError(state){
      state.error = null;
      state.isError = false;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(resCommissionsData.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(resCommissionsData.fulfilled, (state, action) => {
        const {sellers} = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.commissions = sellers;
      })
      .addCase(resCommissionsData.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.error = action.payload;
      })
      .addCase(addCommissionData.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(addCommissionData.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        toast.success("Commission Added");
      })
      .addCase(addCommissionData.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.error = action.payload;
      })
  },
})

export const { ResetCommissionError } = commissionReducer.actions;
export default commissionReducer.reducer;