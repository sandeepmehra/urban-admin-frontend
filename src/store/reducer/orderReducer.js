import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import orderService from '../Services/orderService';

// Get Orders page data
export const resOrderData = createAsyncThunk(
  'order/get',
  async (thunkAPI) => {
    try {
        console.log("res Order DAta called");
        return await orderService.getOrderData();
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)

const initialState = {
    orders: [],
    isError: false,
    isSuccess: false,
    isLoading: true,
    error: null,
}

export const orderReducer = createSlice({
  name: 'order',
  initialState,
  reducers: {
    ResetOrderError(state){
      state.error = null;
      state.isError = false;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(resOrderData.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(resOrderData.fulfilled, (state, action) => {
        const {orders} = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.orders = orders;
      })
      .addCase(resOrderData.rejected, (state, action) => {
        console.log("error: ", action);
        state.isLoading = false;
        state.isError = true;
        state.error = action.payload;
      })
  },
})

export const { ResetOrderError } = orderReducer.actions;
export default orderReducer.reducer;