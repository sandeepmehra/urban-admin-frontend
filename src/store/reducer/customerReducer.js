import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import customerService from '../Services/customerService';

// Get Customer page data
export const resCustomerData = createAsyncThunk(
  'customer/get',
  async (thunkAPI) => {
    try {
        console.log("res customer DAta called");
        return await customerService.getCustomerData();
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)

// Get Review page data
export const resReviewData = createAsyncThunk(
    'Review/get',
    async (customerId, thunkAPI) => {
      try {
          console.log("res getReviewData DAta called");
          return await customerService.getReviewData(customerId);
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
  )
// Updated Customer 
export const putCustomerData = createAsyncThunk(
    'Customer/put',
    async ({id, updateValue}, thunkAPI) => {
      try {
        console.log("updateValue, id in putCustomerData: ", updateValue, id);
        const response = await customerService.updateCustomer(id, updateValue);;
        if(response.status !== 204){
            throw Error(`Customer is not ${updateValue}`);
        }
        if(response.status === 204){
          toast.success(`Customer has been ${updateValue === true ? 'suspended': 'activated' }`);
        }
        return await customerService.getCustomerData();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)
// Updated Review Status 
export const putReviewData = createAsyncThunk(
    'Review/put',
    async ({review_id, approved, id}, thunkAPI) => {
      try {
        console.log("review_id, approved, id in putReview Data: ", review_id, approved, id);
        const response = await customerService.updateReview(review_id, approved);
        if(response.status !== 204){
            throw Error(`Review is not ${approved}`);
        }
        if(response.status === 204){
          toast.success(`Inventory has been ${approved === true ? 'approved': 'rejected'}.`)
        }
        return await customerService.getReviewData(id);
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)
const initialState = {
    customers: [],
    reviews: '',
    isError: false,
    isSuccess: false,
    isLoading: true,
    isReviewLoading: true,
    error: null,
}

export const customerReducer = createSlice({
  name: 'customer',
  initialState,
  reducers: {
    ResetCustomerError(state){
      state.error = null;
      state.isError = false;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(resCustomerData.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(resCustomerData.fulfilled, (state, action) => {
        const {customers} = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.customers = customers;
      })
      .addCase(resCustomerData.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.error = action.payload;
      })
      .addCase(resReviewData.pending, (state) => {
        state.isReviewLoading = true;
      })
      .addCase(resReviewData.fulfilled, (state, action) => {
        state.isReviewLoading = false;
        state.isSuccess = true;
        state.reviews = action.payload;
      })
      .addCase(resReviewData.rejected, (state, action) => {
        state.isReviewLoading = false;
        state.isError = true;
        state.error = action.payload;
      })
      .addCase(putCustomerData.fulfilled, (state, action) => {
        const {customers} = action.payload;
        state.isSuccess = true;
        state.customers = customers;
      })
      .addCase(putCustomerData.rejected, (state, action) => {
        state.isError = true;
        state.error = action.payload;
      })
      .addCase(putReviewData.fulfilled, (state, action) => {  
        state.isSuccess = true;
        state.reviews = action.payload;
      })
      .addCase(putReviewData.rejected, (state, action) => {
        state.isError = true;
        state.error = action.payload;
      })
  },
})

export const { ResetCustomerError } = customerReducer.actions;
export default customerReducer.reducer;