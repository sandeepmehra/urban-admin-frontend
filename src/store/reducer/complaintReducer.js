import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import complaintService from '../Services/complaintService';

// Get Complaint page data
export const resComplaintData = createAsyncThunk(
  'complaint/get',
  async (thunkAPI) => {
    try {
        console.log("res customer DAta called");
        return await complaintService.getComplaintData();
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)
// post complaint response 
export const postComplaintResponse = createAsyncThunk(
    'complaint/response/post',
    async ({complaint_id, new_response}, thunkAPI) => {
      try {
        console.log("updateValue, id in postComplaintResponse: ", new_response, complaint_id);
        const response = await complaintService.postResponse(complaint_id, new_response);
        if(response.status !== 201){
            throw Error(`Response is not added.`);
        }
        return await complaintService.getComplaintData();
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)
// Updated Complaint To Close
export const updateComplaintToClose = createAsyncThunk(
    'complaint/close',
    async ({id}, thunkAPI) => {
      try {
        console.log("id in updateComplaintToClose Data: ", id);
        const response = await complaintService.updateComplaintToCloseAction(id);;
        if(response.status !== 204){
            throw Error(`Status is not updated`);
        }
        return await complaintService.getComplaintData(id);
      } catch (error) {
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj)
      }
    }
)
const initialState = {
    complaints: [],
    isError: false,
    isSuccess: false,
    isLoading: true,
    error: null,
}

export const complaintReducer = createSlice({
  name: 'complaint',
  initialState,
  reducers: {
    ResetComplaintError(state){
      state.error = null;
      state.isError = false;
    }
  },
  extraReducers: (builder) => {
    builder
        .addCase(resComplaintData.pending, (state) => {
            state.isLoading = true;
        })
        .addCase(resComplaintData.fulfilled, (state, action) => {
            const {complaints} = action.payload;
            state.isLoading = false;
            state.isSuccess = true;
            state.complaints = complaints;
        })
        .addCase(resComplaintData.rejected, (state, action) => {
            state.isLoading = false;
            state.isError = true;
            state.error = action.payload;
        })
        .addCase(postComplaintResponse.pending, (state) => {
          state.isResponseLoading = true;
      })
      .addCase(postComplaintResponse.fulfilled, (state, action) => {
          const {complaints} = action.payload;
          state.isResponseLoading = false;
          state.isSuccess = true;
          toast.success("Successfully Responsed.");
          state.complaints = complaints;
      })
      .addCase(postComplaintResponse.rejected, (state, action) => {
          state.isResponseLoading = false;
          state.isError = true;
          state.error = action.payload;
      })
      .addCase(updateComplaintToClose.pending, (state) => {
        state.isLoading = true;
    })
    .addCase(updateComplaintToClose.fulfilled, (state, action) => {
        const {complaints} = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        toast.success("Status is updated");
        state.complaints = complaints;
    })
    .addCase(updateComplaintToClose.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.error = action.payload;
    })
    }
})

export const { ResetComplaintError } = complaintReducer.actions;
export default complaintReducer.reducer;