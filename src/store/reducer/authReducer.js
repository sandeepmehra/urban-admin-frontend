import { createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import Cookies from "js-cookie";
import jwt_decode from 'jwt-decode';
import { isjwt_valid } from '../helper/validation';
import authService from "../Services/authService";
import { toast } from 'react-toastify';


const initState = {
    name: '',
    isAuthenticated: false,
    isError: false,
    isSuccess: false,
    isUpdateSuccess: false,
    error: null,
};
const access_token = Cookies.get('at');
const refresh_token = Cookies.get('rt');
if(access_token && refresh_token){
    isLoggedIn();
}
function isLoggedIn () {
    const decodedAt = jwt_decode(access_token);
    const decodedRt = jwt_decode(refresh_token);
    const rtNotExpired = decodedRt.exp * 1000 > Date.now();
    if((decodedAt !== undefined && decodedRt !== undefined) && isjwt_valid(access_token) && isjwt_valid(refresh_token) &&  rtNotExpired){
        const {sub, name} = decodedAt;
        if (name !== undefined || sub !== undefined) {
            initState.name = name;
            initState.isAuthenticated = true;
        } 
    }
}

// Login with Email and password
export const userLoginByEmail = createAsyncThunk(
    'login', 
    async (formValue, thunkAPI) => {
    try{
        const {data: {refresh_token}, headers:{authorization} } = await authService.userLogin(formValue);
        Cookies.set('at', authorization.split(' ')[1],  { sameSite: 'strict', secure: true });
        Cookies.set('rt', refresh_token, { sameSite: 'strict', secure: true });
        return { at: authorization.split(' ')[1] }; 
    }catch(error){
        const errorObj = {
            message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
            statusCode:(error.response && error.response.status ) || 0
        }
        return thunkAPI.rejectWithValue(errorObj);
    }
});

const authReducer = createSlice({
    name: 'auth',
    initialState: initState,
    reducers: {
        ResetAuthError(state){
            state.error = null;
            state.isError = false;
        },
        ResetAuth(state){
            state.name = '';
            state.isAuthenticated = false;
        },
        SetAuth(state, action){
            const decoded = jwt_decode(action.payload.at);
            const { name } = decoded;
            state.name = name;
            state.isLoading = false;
            state.isSuccess = true;
            state.isAuthenticated=true;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(userLoginByEmail.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(userLoginByEmail.fulfilled, (state, action) => {
                const decoded = jwt_decode(action.payload.at);
                const { name } = decoded;
                state.name = name;
                state.isLoading = false;
                state.isSuccess = true;
                state.isAuthenticated=true;
                toast.success('You are loggedIn.');
            }) 
            .addCase(userLoginByEmail.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.payload;
                toast.error(action.payload.message);
            })
    },
});


export const { ResetAuthError, ResetAuth, SetAuth } = authReducer.actions;
export default authReducer.reducer;