import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import notificationService from '../Services/notificationService';

// Get notifications page data
export const resNotificationData = createAsyncThunk(
  'notifications',
  async (thunkAPI) => {
    try {
        console.log("res notification DAta called");
        return await notificationService.getNotificationData();
    } catch (error) {
      const errorObj = {
          message:(error.response && error.response.data && error.response.data.message) || error.message || error.toString(),
          statusCode:(error.response && error.response.status ) || 0
      }
      return thunkAPI.rejectWithValue(errorObj)
    }
  }
)

const initialState = {
    notifications: [],
    isError: false,
    isSuccess: false,
    isLoading: true,
    error: null,
}

export const notificationReducer = createSlice({
  name: 'notifications',
  initialState,
  reducers: {
    ResetNotificationError(state){
      state.error = null;
      state.isError = false;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(resNotificationData.pending, (state) => {
        state.isLoading = true
      })
      .addCase(resNotificationData.fulfilled, (state, action) => {
        const { notifications } = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.notifications = notifications
      })
      .addCase(resNotificationData.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.error = action.payload
      })
  },
})

export const { ResetNotificationError } = notificationReducer.actions;
export default notificationReducer.reducer;