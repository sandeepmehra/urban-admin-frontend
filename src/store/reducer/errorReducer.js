import { createSlice} from "@reduxjs/toolkit";

const errorReducer = createSlice({
    name: 'globalError',
    initialState: {
        isOpen: false,
        error: null,
    },
    reducers: {
        setError(state, action){
            state.error = action.payload === undefined ? {error:{statusCode:1000}}: action.payload;
            state.isOpen = true;
        },
        hideError(state, action){
            state.error = null;
            state.isOpen = false;
        }
    },
});
  
export const { setError, hideError } = errorReducer.actions;
export default errorReducer.reducer;