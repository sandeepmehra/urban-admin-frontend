import adminApi from '../../api/index';
import Cookies from "js-cookie";

const getNotificationData = async () => {
    const {data} = await adminApi({method: 'get', url: '/notification', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    console.log("getNotificationData resposne: ", data);
    return data;
}

const notificationService = { 
    getNotificationData
}

export default notificationService;