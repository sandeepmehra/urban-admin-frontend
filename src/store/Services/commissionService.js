import adminApi from '../../api/index';
import Cookies from "js-cookie";

const getCommissionData = async () => {
    const {data} = await adminApi({method: 'get', url: '/commissions', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    return data;
}

const addCommission = async (value) => {
    return await adminApi.put(`/commissions`, value, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}

const commissionService = { 
    getCommissionData,
    addCommission
}

export default commissionService;