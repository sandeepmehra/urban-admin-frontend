import { baseURL } from '../../api/index';
import Cookies from "js-cookie";
import { ResetAuth } from '../reducer/authReducer';
import axios from 'axios';

const userLogin = async (formValue) => {
    return await  axios.post(`${baseURL}/login`, formValue, {headers: {'Content-Type': 'application/json'}});
}

export const logoutAction = () => {
    return (dispatch) => {
        if(Cookies.get('at') !== undefined && Cookies.get('rt') !== undefined){
            Cookies.remove('at');
            Cookies.remove('rt');
        }
        return dispatch(ResetAuth());    
    }
}

const authService = {
    userLogin
} 

export default authService;