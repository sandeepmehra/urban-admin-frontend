import adminApi from '../../api/index';
import Cookies from "js-cookie";

const getOrderData = async () => {
    const {data} = await adminApi({method: 'get', url: '/orders', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    console.log("getOrderData resposne: ", data);
    return data;
}

const orderService = { 
    getOrderData
}

export default orderService;