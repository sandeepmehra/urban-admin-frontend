import adminApi from '../../api/index';
import Cookies from "js-cookie";

const getHomeData = async () => {
    const {data} = await adminApi({method: 'get', url: '/home', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    console.log("getHome DAta resposne: ", data);
    return data;
}

const addCommission = async (value) => {
    console.log(value, 'action add commission');
    const data = await adminApi.post(`/commissions`, value, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    return data;
}

const homeService = { 
    getHomeData,
    addCommission
}

export default homeService;