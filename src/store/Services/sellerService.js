import adminApi from '../../api/index';
import Cookies from "js-cookie";

const getSellersData = async () => {
    const {data} = await adminApi({method: 'get', url: '/sellers', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    console.log("getSellersData resposne: ", data);
    return data;
}
const updateSellerApproveOrRejected = async (id, updateValue) => {
    return await adminApi.put(`/seller/${updateValue}`, {"id": String(id)}, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}
const updateUserInfo = async (id, value) => {
    return await adminApi.put(`/seller/${String(id)}`, value, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}

const sellerService = { 
    getSellersData,
    updateSellerApproveOrRejected,
    updateUserInfo
}

export default sellerService;