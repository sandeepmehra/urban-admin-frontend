import adminApi from '../../api/index';
import Cookies from "js-cookie";

const getCustomerData = async () => {
    const {data} = await adminApi({method: 'get', url: '/customer', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    console.log("getCustomerData resposne: ", data);
    return data;
}

const getReviewData = async (customer_id) => {
    const {data} = await adminApi({method: 'get', url: `/review/${customer_id}`, headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    return data;
}

const updateCustomer = async (id, updateValue) => {
    return await adminApi.put(`/customer`, {"customer_id": String(id), "suspend": updateValue}, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}
const updateReview = async (review_id, approved) => {
    return await adminApi.put(`/review`, {"id": String(review_id), "approved": approved}, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}


const customerService = { 
    getCustomerData,
    getReviewData,
    updateCustomer,
    updateReview
}

export default customerService;