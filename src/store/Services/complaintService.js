import adminApi from '../../api/index';
import Cookies from "js-cookie";
import jwt_decode from 'jwt-decode';

const getComplaintData = async () => {
    const {data} = await adminApi({method: 'get', url: '/complaints', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    return data;
}

const postResponse = async (complaint_id, response) => {
    const decoded = jwt_decode(Cookies.get('at'));
    const {sub} = decoded;
    return await adminApi.post(`/complaint/response`, { complaint_id, response, email:sub}, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}

const updateComplaintToCloseAction = async (id) => {
    return await adminApi.put(`/complaint/close`, {id}, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}

const complaintService = { 
    getComplaintData,
    postResponse,
    updateComplaintToCloseAction
}

export default complaintService;