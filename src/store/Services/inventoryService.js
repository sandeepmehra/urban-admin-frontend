import adminApi from '../../api/index';
import Cookies from "js-cookie";

const getInventoryData = async () => {
    const {data} = await adminApi({method: 'get', url: '/inventory', headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    console.log("getInventoryData resposne: ", data);
    return data;
}

const getUnitData = async () => {
    const {data} = await adminApi({method: 'get', url: `/units`, headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
    const options = data.units.map((item, index) => ({
                        "value" : item,
                        "label" : item,
                        "key": index
                    }));
    
    return options;
}

const updateInventoryInfo = async (id, updateValue) => {
    return await adminApi.put(`/inventory/${String(id)}`, updateValue, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}
const updateApprovedOrRejected = async (id, value) => {
    return await adminApi.put(`/inventory/${value}`, {"id": String(id)}, {headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}});
}

const inventoryService = { 
    getInventoryData,
    getUnitData,
    updateInventoryInfo,
    updateApprovedOrRejected
}

export default inventoryService;