import axios from 'axios';
import jwt_decode from 'jwt-decode';
import Cookies from "js-cookie";
import dayjs from 'dayjs';

export const baseURL = process.env.REACT_APP_BASE_API_URL;

const adminApi = axios.create({
    baseURL,
    headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${Cookies.get('at')}`
    },
});

adminApi.interceptors.request.use(async req => {
    let authTokens = Cookies.get('at') ? Cookies.get('at') : null;
    if(!authTokens){
        authTokens = Cookies.get('at') ? Cookies.get('at') : null;
        req.headers.Authorization = `Bearer ${Cookies.get('at')}`
    }
    // console.log("authTokens", authTokens);
    const decodedAt = jwt_decode(authTokens)
    const isExpired = dayjs.unix(decodedAt.exp).diff(dayjs()) < 1;
    // console.log("isExpired", isExpired)
    if(!isExpired) return req
    try {
        const {headers: {authorization}} = await axios({method: 'post', url:`${baseURL}/refresh`, headers: {'Authorization': `Bearer ${Cookies.get('at')}`, 'Content-Type': 'application/json'}, data: {'refresh_token': `${Cookies.get('rt')}`}});
        Cookies.set('at', authorization.split(' ')[1],  { sameSite: 'strict', secure: true });
        // console.log("authorization: ",authorization )
        req.headers.Authorization = `Bearer ${Cookies.get('at')}`
        return req;
    } catch (error) {
        if(error.response && (error.response.status === 401 || error.response.status === 403) ){
            if(Cookies.get('at') !== undefined && Cookies.get('rt') !== undefined){
                Cookies.remove('at');
                Cookies.remove('rt');
            }
        }
    }
    
})

export default adminApi;