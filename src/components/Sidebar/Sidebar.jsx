import React from 'react';
import { useDispatch } from 'react-redux';
import { NavLink } from "react-router-dom";
import style from "./Sidebar.module.css";
import logo from '../../logo.PNG';
import { logoutAction } from '../../store/Services/authService';


const Sidebar = () => {
    const dispatch = useDispatch();
    const logout = async() => {
        dispatch(logoutAction());
    }
    return (
        <>
            <div className={style.sidebar}> 
            <ul>
                    <li className={style.li}>
                        <div className={style.image}>
                            <img src={logo} className={style.sidebar_logo} alt='Logo' />
                        </div>
                    </li>
                    <li className={style.li}>
                        <NavLink 
                            to={"/"} 
                            className={({isActive}) => isActive ? `${style.active}` : undefined}>
                            Dashboard
                        </NavLink>
                    </li>
                    <li className={style.li}>
                        <NavLink to={"customer-account"} className={({isActive}) => isActive ? `${style.active}` : undefined }>
                            Customer Account
                        </NavLink>
                    </li>
                    <li className={style.li}>
                        <NavLink to={"seller-account"} className={({isActive}) => isActive ? `${style.active}` : undefined }>
                            Seller Account
                        </NavLink>
                    </li>
                    <li className={style.li}>
                        <NavLink to={"orders"} className={({isActive}) => isActive ? `${style.active}` : undefined }>
                            Orders
                        </NavLink>
                    </li>
                    <li className={style.li}>
                        <NavLink to={"inventory"} className={({isActive}) => isActive ? `${style.active}` : undefined }>
                            Inventory
                        </NavLink>
                    </li>
                    <li className={style.li}>
                        <NavLink to={"complaints"} className={({isActive}) => isActive ? `${style.active}` : undefined }>
                            Complaints
                        </NavLink>
                    </li>
                    <li className={style.li}>
                        <NavLink to={"notifications"} className={({isActive}) => isActive ? `${style.active}` : undefined }>
                            Notifications
                        </NavLink>
                    </li>
                    <li className={style.li}>
                        <NavLink to={"commission"} className={({isActive}) => isActive ? `${style.active}` : undefined }>
                            Commission
                        </NavLink>
                    </li>
                    <li className={style.li}>
                        <span className={style.logout} onClick={logout}>
                            Logout
                        </span>
                    </li>
                </ul>
            </div>  
        </>
    )
}

export default Sidebar;
