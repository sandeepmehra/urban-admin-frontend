import React from 'react';
import {FaStarHalfAlt, FaStar, FaRegStar} from 'react-icons/fa';

export const ShowStarRating = ({value}) => {
  const totalStars = 5;
  return (
    <div>
      {  [...new Array(totalStars)].map((star, i)=>{
          const activeStars = value;
          const isRatingWithPrecision = activeStars % 1 !== 0;
          const isRatingEqualToIndex = Math.ceil(activeStars) === i + 1;
          const showRatingWithPrecision = isRatingWithPrecision && isRatingEqualToIndex;
        
          if(activeStars >= i+1 && !showRatingWithPrecision){
            return (
              <FaStar fill={'#1F553E'} size={22} key={i} />
          )}
          if(showRatingWithPrecision) {
            return (
              <FaStarHalfAlt fill={"#1F553E"} size={22} key={i}/>
            )
          }
          return (
            <FaRegStar size={22} key={i}/>
          )
        })
      }
    </div>
  )
}
