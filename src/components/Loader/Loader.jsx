import React from 'react'
import style from './Loader.module.css';
import ReactLoading from "react-loading";
export const Loader = ({height}) => {
  return (
    <div className={style.loader} style={{height: `${height}`}}>
      <ReactLoading type="spinningBubbles" color="#1f553e" height={100} width={50} />
    </div>
  )
}
