import React, { useState } from 'react';
import style from '../../pages/Dashboard/Dashboard.module.css';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { addCommonCommissionAction } from '../../store/reducer/homeReducer';
import { toast } from 'react-toastify';

export const CommissionBox = ({current_commission}) => {
    const [commission, setCommission] = useState('');
    const dispatch = useDispatch();
    const handleSubmit = async (e) => {
        e.preventDefault();
        if(commission !== ''){
            dispatch(addCommonCommissionAction({commission_percentage: Number(commission)}));
        }else{
            toast.info("Commission is empty");
        }
        setCommission('');
    }
    
  return (
    <>
        <div className={style.card_one}>
            <div className={style.card_one_item}>
                <div className={style.heading_item}>
                    Current commission set to <h5 className={style.heading_h5}>{current_commission}%</h5> 
                </div>
                <h2 className={style.pt5}>Update Commission</h2>
                <form onSubmit={handleSubmit}>
                    <input 
                        type="input" 
                        className={style.box_input} 
                        value={commission} 
                        onChange={e => setCommission(e.target.value)}
                    />
                    <button 
                        className={style.save_btn}
                    >
                        Save
                    </button> 
                </form>      
            </div>
            <div className={style.view_more}>
                <Link to={"/commission"}>
                    <span className={style.view_more_a}>View more</span>
                </Link>
            </div>
        </div>
    </>
  )
}
