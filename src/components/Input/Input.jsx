import { ErrorMessage } from '@hookform/error-message';

const Input = ({register, ...rest}) => {
    return (
        <>
            <input {...rest} {...register(rest.name, {required: true})} />
            <div className={rest.error_body}>
            <ErrorMessage
                render={({ message }) => (
                    <small className={rest.error_style}>{message}</small>
                )}
                errors={rest.errors}
                name={rest.name}
            />
            </div>
        </>
    )
}

export default Input;
