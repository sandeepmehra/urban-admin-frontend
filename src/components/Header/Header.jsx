import React from 'react';
import style from "./Header.module.css";
import FeatherIcon from 'feather-icons-react';
import { useSelector } from 'react-redux';

const Header = () => {
    const {name} = useSelector((state) => state.auth);
    return (
        <>
           <nav className={style.header}>
                <div className={style.profile}>
                    <div className={style.profile_circle}>
                        <FeatherIcon icon="user" fill="white" size={24} />
                    </div>
                    <div className={style.profile_name}>
                        <small>Hi! {name !== undefined ? name : "There"}</small>
                    </div>
                </div>    
           </nav>
        </>
    )
}

export default Header;
