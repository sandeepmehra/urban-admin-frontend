import React, { useEffect } from 'react';
import FeatherIcon from 'feather-icons-react';
import { Link, useNavigate } from 'react-router-dom';

export const Accordion = ({setIsOpen, id, isOpen, value, setDefaultOpen}) => {
    const navigate = useNavigate();
    const showEditDiv = (id, value) => {
        if(value){
            setDefaultOpen('');
            setIsOpen([])
            return;
        }
        if(isOpen.includes(id)) {
            setIsOpen([...isOpen.filter(listId => listId !== id )]);
            return;
        }
        setIsOpen([id]); 
    };
    useEffect(()=>{
        if(id === isOpen) {
            navigate(`${id}`, {replace: true})
        }
        if(isOpen.length === 0){
            navigate('', { replace: true });
        }
    },[isOpen, navigate, id]);
  return (
    <>
        <span onClick={() => showEditDiv(id, value)}>
            <Link to={`${id}`}>
                {isOpen.includes(id) !== true ? (
                    <FeatherIcon
                        icon="chevron-down" 
                        style={{'color': '#1F553E'}}
                    />)  
                    :      
                    (<FeatherIcon 
                        icon="chevron-up"
                        style={{'color': '#1F553E'}}
                    />)
                }
            </Link>
        </span>
    </>
  )
}
