export const customStyles = {
    option: (provided, state) => ({
      ...provided,
      borderBottom: '1px solid black',
      padding: 5,
    }),
    valueContainer: (provided) => ({
        ...provided,
        minHeight: '20px',
        padding: '0px',
        fontSize: '10px',
    }),
    control: (provided) => ({
        ...provided,
        borderColor: '#000000',
        minHeight: 24,
        padding: '0px',
        boxShadow: '1px #000000',
        '&:hover': {
            borderColor:'#000000',
        }
    }),
    container: provided => ({
        ...provided,
        color: '#000000',
        width: 150,
        padding: 0,
        minHeight: 24,
        fontSize: '10px',
    }),
    dropdownIndicator: (styles) => ({
        ...styles,
        color: '#000000',
        padding: '0px',
        paddingLeft: '0px',
        paddingTop: '0px',
        paddingRight: '0px',
        paddingBottom: '0px',
        fontSize: '10px',
    }),
    clearIndicator: (styles) => ({
        ...styles,
        padding: '0px',
        paddingLeft: '0px',
        paddingTop: '0px',
        paddingRight: '0px',
        paddingBottom: '0px',
        fontSize: '10px',
    }),
    input:(provided)=>({
        ...provided,
        padding:'0px',
        margin: '0px',
        color: 'transparent',
        fontSize: '10px',
    }),
    singleValue: (provided, state) => {
      const opacity = state.isDisabled ? 0.5 : 1;
      const transition = 'opacity 300ms';
  
      return { ...provided, opacity, transition };
    },
    menuOptions:(provided, state) => {
        return { ...provided, color: state.isFocused ? 'blue' : '' };
    },
    menuPortal: (provided, state) => {
        return { ...provided, color: state.isFocused ? 'blue' : '' };
    }
}