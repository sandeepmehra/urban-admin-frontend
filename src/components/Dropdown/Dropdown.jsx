import React from 'react';
import Select from 'react-select';
import { customStyles } from './SelectStyle';

const Dropdown = ({optionsData, chooseUnitData, menuHeight, placeholder, defaultValue }) => {
    
  return (
    <div style={{fontsize: '10px'}}>
        <Select 
            autoFocus={true}
            styles={customStyles} 
            maxMenuHeight={menuHeight} 
            options={optionsData} 
            placeholder={placeholder} 
            defaultValue={defaultValue}
            onChange={chooseUnitData}
        />
    </div>
  )
}

export default Dropdown;